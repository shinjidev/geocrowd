from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    #index
    url(r'^$', TemplateView.as_view(template_name="index.html"),
        name='index'),

    #apps
    url(r'^check-in/', include('users.checkin.urls')),
    url(r'^comments/', include('comment.urls')),
    url(r'^join/', include('users.join.urls')),
    url(r'^login/', include('users.login.urls')),
	url(r'^offers/', include('offers.urls')),
	url(r'^places/', include('places.urls')),
    url(r'^tags/', include('tags.urls')),
    url(r'^users/', include('users.urls')),

	#admin
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
)

if settings.DEBUG:
    from django.views.static import serve
    urlpatterns += patterns('',
        url(r'^static/(?P<path>.*)$',
            serve, {
                'document_root': settings.STATIC_ROOT,
                'show_indexes': True
            }
        ),
        url(r'^media/(?P<path>.*)$',
            serve, {
                'document_root': settings.MEDIA_ROOT,
                'show_indexes': True
            }
        ),
    )
