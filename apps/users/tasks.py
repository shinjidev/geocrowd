from celery.task import task

from users.models import UserProfile

@task
def reset_week_points():
	profiles = UserProfile.objects.all()
	for profile in profiles:
		profile.experience_points_week = 0
		profile.num_visited_places_week = 0
		profile.save() 