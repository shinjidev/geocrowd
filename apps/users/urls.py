from django.conf.urls import patterns, include, url

from users import resources
from users.badge_resources import resources as bg_resources

urlpatterns = patterns('',

    url(r'^(?P<user_id>[-\w]+)/places/(?P<place_id>[-\w]+)/friends/aftervisit/$',
        resources.FriendsAfterVisit.as_view(),
        name='aftervisit'),
	url(r'^(?P<user_id>[-\w]+)/friends/timeline/$',
        resources.FriendsTimelineCollection.as_view(),
        name='timeline'),
	url(r'^(?P<user_id>[-\w]+)/friends/ranking/$',
        resources.FriendsRankingCollection.as_view(),
        name='ranking'),
    url(r'^(?P<user_id>[-\w]+)/badges/$',
        bg_resources.BadgesCollection.as_view(),
        name='badges'),
    url(r'^(?P<user_id>[-\w]+)/$',
        resources.UserResource.as_view(),
        name='users'),

)

