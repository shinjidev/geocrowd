from django.conf.urls import patterns, include, url
from users.login.resources import LoginController

urlpatterns = patterns('',
	
	url(r'^$',
        LoginController.as_view(),
        name='login'),

)