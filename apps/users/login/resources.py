from ordereddict import OrderedDict

from django.utils import simplejson

from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseNotFound
from django.contrib.auth.models import User

from rest.http import HttpResponseCreated
from rest.http import HttpResponseConflict

from rest.views import ControllerResourceView

from rest.utils import render_as_json
from rest.utils import json_error_document

from users.models import UserProfile
from users.resources import UserResource
from users import strings

class LoginController(ControllerResourceView):
    """
    Class that implements the logic to login user,
    implemented as a REST resource.
    """

    def run(self, request, *args, **kwargs):
        
        data = simplejson.loads(request.raw_post_data)
        for entry in ['username','password']:
            if data[entry] == "":
                error = OrderedDict()
                message = unicode(" ".join(["The",entry,"field is mandatory!"]))
                error['message'] = message
                error_list = [error]
                return HttpResponseConflict(
                    content=json_error_document(error_list),
                    content_type='application/json',
                )

        try:
            user = User.objects.get(username=data['username'])
            if not user.check_password(data['password']):
                error = OrderedDict()
                message = unicode(strings.WRONG_PASSWORD)
                error['message'] = message
                error_list = [error]
                return HttpResponseConflict(
                    content=json_error_document(error_list),
                    content_type='application/json',
                )
            access_token = user.access_token
            profile = user.profile
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = OrderedDict()
            json_document['data']['token'] = \
                UserResource.access_token_json_representation(access_token,False)
            json_document['data']['user'] = UserResource.json_representation(profile,False)
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

        except User.DoesNotExist:
            error = OrderedDict()
            message = unicode(strings.NON_EXISTANT_USER % data['username']) 
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json',
            )