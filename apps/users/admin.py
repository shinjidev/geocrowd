from django.contrib import admin
from users.models import *

class UserAccessTokenAdmin(admin.ModelAdmin):
	readonly_fields=('uuid','creation_date',)

admin.site.register(UserAccessToken,UserAccessTokenAdmin)

class UserProfileAdmin(admin.ModelAdmin):
	list_display=('__unicode__','get_photo',)
	readonly_fields=(
		'experience_points',
		'experience_points_week',
		'num_validated_places',
		'num_validated_tags',
		'num_visited_places',
		'num_visited_places_week',
	)

admin.site.register(UserProfile,UserProfileAdmin)

class UserFriendAdmin(admin.ModelAdmin):
	readonly_fields=('date',)

admin.site.register(UserFriend,UserFriendAdmin)

class UserVisitPlaceAdmin(admin.ModelAdmin):
	list_display=('__unicode__','experience_points',)
	readonly_fields=('date',)

admin.site.register(UserVisitPlace,UserVisitPlaceAdmin)


#USER ADMIN EXTENSION
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
import comment
import offers
import places
import tags

admin.site.unregister(User)

class CommentInline(admin.StackedInline):
    model = comment.models.Comment
    readonly_fields=(
    	'description',
    	'place',
    	'likes',
    	'dislikes',
    	'visibility',
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class ModificationPlaceInline(admin.StackedInline):
    model = places.models.ModificationPlace
    readonly_fields=(
    	'modification_type',
    	'place',
    	'name',
    	'description',
    	'coordinates',
    	'address',
    	'country',
    	'place_type',
    	'get_photo',
    	'photo'
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class OfferInline(admin.StackedInline):
    model = offers.models.Offer
    readonly_fields=(
    	'description',
    	'place',
    	'likes',
    	'dislikes',
    	'visibility',
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class PlaceInline(admin.StackedInline):
    model = places.models.Place
    readonly_fields=(
    	'name',
    	'description',
    	'coordinates',
    	'state',
    	'address',
    	'country',
    	'place_type',
    	'likes',
    	'dislikes',
    	'visibility',
    	'get_photo',
    	'photo'
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class TagInline(admin.StackedInline):
    model = tags.models.Tag
    readonly_fields=(
    	'name',
    	'state',
    	'place',
    	'yeah',
    	'nay',
    	'visibility',
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    readonly_fields=(
    	'gender',
        'kharma_points',
		'experience_points',
		'experience_points_week',
		'num_validated_places',
		'num_validated_tags',
		'num_visited_places',
		'num_visited_places_week',
		'get_photo',
    	'photo'
	)

class UserVisitPlaceInline(admin.StackedInline):
	model = UserVisitPlace
	readonly_fields=(
		'place',
		'experience_points',
		'date',
		)
	fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
	extra = 0

class ValidationModificationPlaceInline(admin.StackedInline):
    model = places.models.ValidationModificationPlace
    readonly_fields=(
    	'modification_place',
    	'date',
    	'vote',
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class ValidationPlaceInline(admin.StackedInline):
    model = places.models.ValidationPlace
    readonly_fields=(
    	'place',
    	'date',
    	'vote',
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class VoteCommentInline(admin.StackedInline):
    model = comment.models.VoteComment
    readonly_fields=(
    	'comment',
    	'date',
    	'vote',
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class VoteOfferInline(admin.StackedInline):
    model = offers.models.VoteOffer
    readonly_fields=(
    	'offer',
    	'date',
    	'vote',
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class VotePlaceInline(admin.StackedInline):
    model = places.models.VotePlace
    readonly_fields=(
    	'place',
    	'date',
    	'vote',
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class ValidationTagInline(admin.StackedInline):
    model = tags.models.ValidationTag
    readonly_fields=(
    	'tag',
    	'date',
    	'vote',
    	)
    fieldsets = (
        (None, {
            'fields': ()
        }),
        ('Details', {
            'classes': ('collapse',),
            'fields': readonly_fields
        })
    )
    extra = 0

class StaffAdmin(UserAdmin):
    inlines = [
    	UserProfileInline,
    	CommentInline,
    	VoteCommentInline,
    	OfferInline,
    	VoteOfferInline,
    	PlaceInline,
    	ModificationPlaceInline,
    	ValidationModificationPlaceInline,
    	ValidationPlaceInline,
    	VotePlaceInline,
    	TagInline,
    	ValidationTagInline,
    	UserVisitPlaceInline,
    ]
    # provide further customisations here

admin.site.register(User,StaffAdmin)