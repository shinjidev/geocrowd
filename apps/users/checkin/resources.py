from ordereddict import OrderedDict
import django.utils.simplejson as json
from datetime import datetime, date, time

from django.http import HttpResponseBadRequest
from django.contrib.auth.models import User
from django.conf import settings

from rest.utils import render_as_json
from rest.utils import json_error_document
from rest.auth import RESTfulBasicOrDjangoAuthenticationMixIn
from rest.http import HttpResponseConflict
from rest.http import HttpResponse

from rest.views import ControllerResourceView

from places.models import Place
from users.models import UserVisitPlace
from users import strings

class UserVisitPlaceController(
    RESTfulBasicOrDjangoAuthenticationMixIn,
    ControllerResourceView
):
    
    badge_list = []

    def award_badge(self,sender,**kwargs):
        from users.badge_resources.resources import BadgeResource
        self.badge_list.append(BadgeResource.short_json_representation(
            kwargs['badge'],False)
        )

    def run(self, request, *args, **kwargs):
        from badges.signals import badge_awarded
        from users.meta_badges import Turist, Veteran
        badge_awarded.connect(self.award_badge, sender=Turist)
        badge_awarded.connect(self.award_badge, sender=Veteran)

        data = json.loads(request.raw_post_data)
        data = data['check-in']
        val_keys, entry = self.validate_fields(data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        try:
            place = Place.objects.get(
                id=data['place']['id']
            )
        except Place.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_PLACE % data['place']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            user = User.objects.get(
                id=data['user']['id']
            )
        except User.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_USER % data['user']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            visit = UserVisitPlace.objects.get(
                user=user.id,
                place=place.id,
                date__range= (
                    datetime.combine(date.today(), time.min), 
                    datetime.combine(date.today(), time.max)
                )
            )
            error = OrderedDict()
            error['message'] = \
                unicode(strings.VISIT_ALREADY_EXISTS % 
                    (data['place']['id'],data['user']['id'],visit.date.time()))
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        except UserVisitPlace.DoesNotExist:
            experience_points = settings.EXPERIENCE_POINTS_EARNED['check-in']
            visit = UserVisitPlace.create_new_visit(
                place=place,
                user=user,
                experience_points=experience_points
            )
            user.profile.experience_points+=experience_points
            user.profile.experience_points_week+=experience_points
            user.profile.num_visited_places+=1
            user.profile.num_visited_places_week+=1
            user.profile.save()

            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['badges'] = self.badge_list
            json_document['experience_points'] = experience_points
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.VISIT_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif (entry == 'user' or entry == 'place') and data[entry]['id']=="":
                return False, entry
        return True, None

    @staticmethod
    def visited_today(place, user):
        visits_list = UserVisitPlace.objects.filter(place=place,user=user,date__gte=date.today)
        if visits_list:
            return True
        return False