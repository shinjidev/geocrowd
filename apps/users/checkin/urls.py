from django.conf.urls import patterns, include, url

from users.checkin import resources

urlpatterns = patterns('',
    url(r'^$',
        resources.UserVisitPlaceController.as_view(),
        name='check-in'),

)