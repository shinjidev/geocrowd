import badges
from users.models import UserProfile

class Veteran(badges.MetaBadge):
        goal = 100

        id = "veteran"
        model = UserProfile
        one_time_only = True

        title = "Veteran"
        description = "More than %s XP points" % goal
        level = "1"
        
        def get_user(self, instance):
            return instance.user
        
        def check_xp_points(self, instance):
            return instance.experience_points > self.goal

class Turist(badges.MetaBadge):
        goal = 8

        id = "turist"
        model = UserProfile
        one_time_only = True

        title = "Turist"
        description = "At least %s visited places." % goal
        level = "1"

        progress_start = 0
        progress_finish = goal

        def get_progress(self, instance):
            return instance.num_visited_places
        
        def get_user(self, instance):
            return instance.user
        
        def check_num_visited_places(self, instance):
            return instance.num_visited_places >= self.goal

class PlaceValidator(badges.MetaBadge):
        goal = 5

        id = "placevalidator"
        model = UserProfile
        one_time_only = True

        title = "PlaceValidator"
        description = "At least %s validated places." % goal
        level = "1"

        progress_start = 0
        progress_finish = goal

        def get_progress(self, instance):
            return instance.num_validated_places
        
        def get_user(self, instance):
            return instance.user
        
        def check_num_validated_places(self, instance):
            return instance.num_validated_places >= self.goal

class TagValidator(badges.MetaBadge):
        goal = 7

        id = "tagvalidator"
        model = UserProfile
        one_time_only = True

        title = "TagValidator"
        description = "At least %s validated tags." % goal
        level = "1"

        progress_start = 0
        progress_finish = goal

        def get_progress(self, instance):
            return instance.num_validated_tags
        
        def get_user(self, instance):
            return instance.user
        
        def check_num_validated_tags(self, instance):
            return instance.num_validated_tags >= self.goal

from comment.models import Comment
class Commentator(badges.MetaBadge):
        goal = 20

        id = "commentor"
        model = Comment
        one_time_only = True

        title = "Commentarist"
        description = "More than %s comments made." % goal
        level = "1"

        progress_start = 0
        progress_finish = goal

        def get_progress(self, user):
            num_comments = Comment.objects.filter(user=user).count()
            return num_comments
        
        def get_user(self, instance):
            return instance.user
        
        def check_num_comments(self, instance):
            num_comments = Comment.objects.filter(user=instance.user).count()
            return num_comments >= self.goal

from offers.models import Offer
class Salesman(badges.MetaBadge):
        goal = 20

        id = "salesman"
        model = Offer
        one_time_only = True

        title = "Salesman"
        description = "More than %s offers made." % goal
        level = "1"

        progress_start = 0
        progress_finish = goal

        def get_progress(self, user):
            num_offers = Offer.objects.filter(user=user).count()
            return num_offers
        
        def get_user(self, instance):
            return instance.user
        
        def check_num_offers(self, instance):
            num_offers = Offer.objects.filter(user=instance.user).count()
            return num_offers >= self.goal