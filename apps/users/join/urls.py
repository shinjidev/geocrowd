from django.conf.urls import patterns, include, url
from users.join.resources import RegistrationController

from django.views.generic import TemplateView

urlpatterns = patterns('',
	
	url(r'^$',
        TemplateView.as_view(template_name="join.html"),
        name='join'),
	url(r'^add/',
        RegistrationController.as_view(),
        name='add'),

)