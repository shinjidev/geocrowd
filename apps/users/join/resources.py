from ordereddict import OrderedDict

from django.utils import simplejson as json

from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseNotFound
from django.contrib.auth.models import User

from rest.http import HttpResponseCreated
from rest.http import HttpResponseConflict

from rest.views import ControllerResourceView

from rest.utils import render_as_json
from rest.utils import json_error_document

from users.models import UserProfile
from users.resources import UserResource
from users import strings

class RegistrationController(ControllerResourceView):
    """
    Class that implements the logic to register a new user,
    implemented as a REST resource.
    """

    def run(self, request, *args, **kwargs):
        data = json.loads(request.POST['user'])
        print data
        cleaned_data = data["user"]
        for entry in ['username','password','email']:
            if cleaned_data[entry] == "":
                error = OrderedDict()
                message = unicode(" ".join(["The",entry,"field is mandatory!"]))
                error['message'] = message
                error_list = [error]
                return HttpResponseBadRequest(
                    content=json_error_document(error_list),
                    content_type='application/json',
                )

        if self.valid_username(cleaned_data['username']):
            try: 
                photo = request.FILES['photo']
            except:
                photo = None
            user = UserProfile.create_user_with_tokens(
                first=cleaned_data['first'],
                last=cleaned_data['last'],
                usname=cleaned_data['username'],
                email=cleaned_data['email'],
                password=cleaned_data['password'],
                gender=cleaned_data['gender'],
                photo=photo
            )
            access_token = user.access_token
            profile = user.profile
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = OrderedDict()
            json_document['data']['token'] = \
                UserResource.access_token_json_representation(access_token,False)
            json_document['data']['user'] = UserResource.short_json_representation(profile,False)
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )
        else:
            # return username already in use
            error = OrderedDict()
            error['message'] = unicode(strings.USERNAME_ALREADY_IN_USE)
            error_list = [error]
            return HttpResponseConflict(
                json_error_document(error_list)
            )

    def valid_username(self,username):
        # check if email is already in use
        if User.objects.filter(username=username).exists():
            return False
        return True