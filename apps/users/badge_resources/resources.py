#! -*- coding: utf-8 -*-

from ordereddict import OrderedDict

from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.conf import settings

from rest.views import ResourceView, CollectionResourceView

from rest.utils import render_as_json
from rest.utils import json_error_document
from rest.auth import RESTfulBasicOrDjangoAuthenticationMixIn

from users import strings
from badges.models import BadgeToUser, Badge

class BadgeResource(
    RESTfulBasicOrDjangoAuthenticationMixIn,
    ResourceView
):
    """
    Class that implements the logic for the user REST resource.
    """

    def get(self, request, *args, **kwargs):
        """
        Returns a required user as a REST (JSON) resource.
        """

        badge_id = kwargs.get('badge_id', 0)
        try:
            badge = Badge.objects.get(id=badge_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                UserResource.json_representation(profile,False)
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

        except Badge.DoesNotExist:
            error = OrderedDict()
            message = unicode(strings.NON_EXISTANT_BADGE % badge_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json',
            )

    @staticmethod
    def json_representation(badge, render_json=True):
        document = OrderedDict()
        document['id'] = badge.id
        document['name'] = badge.title
        document['description'] = badge.description
        document['level'] = badge.level
        document['icon'] = '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            badge.icon.url
        )
        if render_json:
            return render_as_json(document)
        else:
            return document

    @staticmethod
    def short_json_representation(badge, render_json=True):
        document = OrderedDict()
        document['id'] = badge.id
        document['name'] = badge.title
        document['icon'] = '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            badge.icon.url
        )
        if render_json:
            return render_as_json(document)
        else:
            return document

class BadgesCollection(
    RESTfulBasicOrDjangoAuthenticationMixIn,
    CollectionResourceView
):
    """
    Class that gives a list of the badges won by a user.
    """

    def get(self, request, *args, **kwargs):

        self.user_id = kwargs.get('user_id', 0)
        try:
            user = User.objects.get(id=self.user_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = OrderedDict()
            json_document['data']['badges'] = \
                BadgesCollection.json_representation(user,False)
            json_document['link'] = \
                self.get_restful_link_metadata(rel='self')
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

        except User.DoesNotExist:
            error = OrderedDict()
            message = unicode(strings.NON_EXISTANT_USER % self.user_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json',
            )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('badges',kwargs={'user_id':self.user_id})
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.BADGES)
        metadata['type'] = 'application/json'
        return metadata

    @staticmethod
    def json_representation(user,render_json=True):

        document = OrderedDict()
        badge_list = []
        badges = BadgeToUser.objects.select_related().filter(
            user=user.id
        )
        for badge in badges:
            badge_list.append({"badge":
                BadgeResource.short_json_representation(
                    badge.badge,
                    False
                )}
            )

        return badge_list