from django.utils.translation import ugettext_lazy as _

VISIT_MANDATORY_FIELDS = ['user','place']

USER_PROFILE_USER = _(u'User')
USER_PROFILE_GENDER = _(u'Gender')
USER_PROFILE_PHOTO = _(u'Profile Picture')
USER_PROFILE_KHARMA_POINTS = _(u'Kharma points')
USER_PROFILE_XP_POINTS = _(u'Experience points')
USER_PROFILE_XP_POINTS_WEEK = _(u'Experience points in a week')
USER_PROFILE_VAL_PLACES = _(u'Number validated places')
USER_PROFILE_VAL_TAGS = _(u'Number validated tags')
USER_PROFILE_VISITED_PLACES = _(u'Number visited places')
USER_PROFILE_VISITED_PLACES_WEEK = _(u'Number visited places in a week')
USER_PROFILE_VERBOSE_NAME = _(u'User profile')
USER_PROFILE_VERBOSE_NAME_PLURAL = _(u'User profiles')

USER_ACCESS_TOKEN = _(u'Access token')
USER_AT_USER = _(u'User')
USER_AT_CREATION_DATE = _(u'Creation date')
USER_AT_EXPIRATION_DATE = _(u'Expiration date')
USER_AT_VERBOSE_NAME = _(u'Access token')
USER_AT_VERBOSE_NAME_PLURAL = _(u'Access tokens')

USER_FRIEND = _(u'Friend')
USER_FRIEND_DATE = _(u'Date')
USER_FRIEND_VERBOSE_NAME = _(u'Friend')
USER_FRIEND_VERBOSE_NAME_PLURAL = _(u'Friends')

USER_VISIT_PLACE = _(u'Place')
USER_VISIT_DATE = _(u'Date')
USER_VISIT_VERBOSE_NAME = _(u'Visit')
USER_VISIT_VERBOSE_NAME_PLURAL = _(u'Visits')

GENDER_MALE = _(u'Male')
GENDER_FEMALE = _(u'Female')
GENDER_UNDISCLOSED = _(u'Prefer not to say')

USERNAME_ALREADY_IN_USE = _(u'Username already in use!')
NON_EXISTANT_USER = _(u'User %s does not exist!')
WRONG_PASSWORD = _(u'Wrong password!')

VISIT_ALREADY_EXISTS = \
	_(u'A visit for today, already exists for this place %s and user %s, at %s!')
NON_EXISTANT_PLACE = _(u'Place %s does not exist!')

FRIENDS_RANKING = \
	_(u'A user\'s friends ranking by experience points won in a week.')
FRIENDS_TIMELINE = \
	_(u'A user\'s friends timeline with their latest activities.')
FRIENDS_AFTER_VISIT = \
	_(u'A user\'s friends activities after visiting a place.')

BADGES = _(u'A list of all the badges won by a user.')
NON_EXISTANT_BADGE = _(u'Badge %s does not exist!')