import mimetypes
from ordereddict import OrderedDict

from django_extensions.db.fields import UUIDField
from sorl.thumbnail.fields import ImageField
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.core.urlresolvers import reverse

from common.datastructures.enumeration import Enumeration
from common.utils.photos import uuid_based_picture_name
from common.utils.date import datetime_plus_days

from users import strings

class UserAccessToken(models.Model):
    user = models.OneToOneField(User,
        verbose_name=strings.USER_AT_USER,
        related_name='access_token',
        primary_key = True
    )

    uuid = UUIDField(
        verbose_name=strings.USER_ACCESS_TOKEN,
        version=4
    )

    creation_date = models.DateTimeField(
        verbose_name=strings.USER_AT_CREATION_DATE,
        auto_now_add=True,
        editable=False
    )

    expiration_date = models.DateTimeField(
        verbose_name=strings.USER_AT_EXPIRATION_DATE,
        default=datetime_plus_days(days=5),
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = strings.USER_AT_VERBOSE_NAME
        verbose_name_plural = strings.USER_AT_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s (%s)' % (self.user.get_full_name(),self.user.username,)


class UserProfile(models.Model):
    """
    Class that expands the Django Auth-User model,
    containing needed information for the program.
    """

    GENDER_CHOICES = Enumeration([
        (1, 'M', strings.GENDER_MALE),
        (2, 'F', strings.GENDER_FEMALE),
    ])

    user = models.OneToOneField(User,
        verbose_name=strings.USER_PROFILE_USER,
        related_name='profile',
    )

    gender = models.PositiveIntegerField(
        verbose_name=strings.USER_PROFILE_GENDER,
        choices=GENDER_CHOICES,
        default=3,
    )

    photo = ImageField(
        verbose_name=strings.USER_PROFILE_PHOTO,
        upload_to=uuid_based_picture_name('photos/user_profile'),
        blank=True,
        null=True,
    )

    kharma_points = models.IntegerField(
        verbose_name=strings.USER_PROFILE_KHARMA_POINTS,
        default=0,
        null=True
    )

    experience_points = models.PositiveIntegerField(
        verbose_name=strings.USER_PROFILE_XP_POINTS,
        editable=False,
        default=0
    )

    experience_points_week = models.PositiveIntegerField(
        verbose_name=strings.USER_PROFILE_XP_POINTS_WEEK,
        editable=False,
        default=0
    )

    num_validated_places = models.PositiveIntegerField(
        verbose_name=strings.USER_PROFILE_VAL_PLACES,
        editable=False,
        default=0
    )

    num_validated_tags = models.PositiveIntegerField(
        verbose_name=strings.USER_PROFILE_VAL_TAGS,
        editable=False,
        default=0
    )

    num_visited_places = models.PositiveIntegerField(
        verbose_name=strings.USER_PROFILE_VISITED_PLACES,
        editable=False,
        default=0
    )

    num_visited_places_week = models.PositiveIntegerField(
        verbose_name=strings.USER_PROFILE_VISITED_PLACES_WEEK,
        editable=False,
        default=0
    )


    class Meta:
        verbose_name = strings.USER_PROFILE_VERBOSE_NAME
        verbose_name_plural = strings.USER_PROFILE_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s (%s)' % (self.user.get_full_name(),self.user.username,)

    @staticmethod
    def create_user_with_tokens(
        first,
        last,
        usname,
        email,
        password,
        gender,
        photo
    ):
        """
        Method for creating a user, bind to userprofile.
        """

        # create a new User
        new_user = User.objects.create_user(
            usname,
            email,
            password
        )
        new_user.first_name = (first)
        new_user.last_name = (last)
        new_user.save()
        # bind new_user to a UserProfile
        new_profile = UserProfile(
            user=new_user,
            photo=photo
        )
        new_profile.gender = (UserProfile.GENDER_CHOICES.get_value(gender))
        new_profile.save()

        new_access_token = UserAccessToken(user=new_user)
        new_access_token.save()

        return new_user

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = self.user.get_full_name()
        metadata['type'] = 'application/json'
        return metadata

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('users', kwargs={'user_id': self.user.pk})
        )

    def get_photo_restful_url(self):
        return '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            self.photo.url
        )

    def get_photo_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_photo_restful_url()
        metadata['rel'] = rel
        metadata['title'] = self.photo.file.name.split('/')[-1]
        metadata['type'] = mimetypes.guess_type(self.photo.file.name)[0]
        return metadata

    def get_photo(self):
        if self.photo:
            return u'<img src="%s" height="75"/>' \
                % self.get_photo_restful_url()
        else:
            return '(No photo)'

    def get_kharma_average(self):
        return (settings.PERCENTAGE_KHARMA_POINTS['kharma']*self.kharma_points+
            settings.PERCENTAGE_KHARMA_POINTS['validated_places']*self.num_validated_places+
            settings.PERCENTAGE_KHARMA_POINTS['validated_tags']*self.num_validated_tags+
            settings.PERCENTAGE_KHARMA_POINTS['visited_places']*self.num_visited_places
        )
            
    get_photo.allow_tags = True

class UserFriend(models.Model):
    """
    Class that stores the relationship between to users.
    """

    user = models.ForeignKey(User,
        verbose_name=strings.USER_PROFILE_USER,
        related_name='user',
    )

    friend = models.ForeignKey(User,
        verbose_name=strings.USER_FRIEND,
        related_name='friend',
    ) 

    date = models.DateTimeField(
        verbose_name=strings.USER_FRIEND_DATE,
        auto_now_add=True,
        editable=False
    )

    class Meta:
        verbose_name = strings.USER_FRIEND_VERBOSE_NAME
        verbose_name_plural = strings.USER_FRIEND_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s friend with %s' % (self.user.get_full_name(), 
            self.friend.get_full_name(),)


from places.models import Place

class UserVisitPlace(models.Model):
    """
    Class that stores the check-in history of a user.
    """

    user = models.ForeignKey(User,
        verbose_name=strings.USER_PROFILE_USER,
    )

    place = models.ForeignKey(Place,
        verbose_name=strings.USER_VISIT_PLACE,
    )

    experience_points = models.PositiveIntegerField(
        verbose_name=strings.USER_PROFILE_XP_POINTS,
        default=10,
    )

    date = models.DateTimeField(
        verbose_name=strings.USER_VISIT_DATE,
        auto_now_add=True,
        editable=False
    )

    class Meta:
        verbose_name = strings.USER_VISIT_VERBOSE_NAME
        verbose_name_plural = strings.USER_VISIT_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s visits %s' % (self.user.get_full_name(), 
            self.place.name,)

    @staticmethod
    def create_new_visit(
        place,
        user,
        experience_points
        ):
        new_visit = UserVisitPlace(
            place=place,
            user=user,
            experience_points=experience_points
        )
        new_visit.save()
        return new_visit

import users.meta_badges