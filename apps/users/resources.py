#! -*- coding: utf-8 -*-

from ordereddict import OrderedDict

from django.db.models import Q
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.conf import settings

from rest.views import ResourceView, CollectionResourceView

from rest.utils import render_as_json
from rest.utils import json_error_document
from rest.auth import RESTfulBasicOrDjangoAuthenticationMixIn

from places.models import Place
from users.models import UserProfile, UserAccessToken, UserFriend, UserVisitPlace
from users import strings

class UserResource(
    RESTfulBasicOrDjangoAuthenticationMixIn,
    ResourceView
):
    """
    Class that implements the logic for the user REST resource.
    """

    def get(self, request, *args, **kwargs):
        """
        Returns a required user as a REST (JSON) resource.
        """

        user_id = kwargs.get('user_id', 0)
        try:
            profile = UserProfile.objects.get(user__id=user_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                UserResource.json_representation(profile,False)
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

        except UserProfile.DoesNotExist:
            error = OrderedDict()
            message = unicode(strings.NON_EXISTANT_USER % user_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json',
            )

    @staticmethod
    def json_representation(profile, render_json=True):
        # Gender subdoc
        gender_subdoc = OrderedDict()
        gender_subdoc['value'] = profile.gender
        gender_subdoc['key'] = UserProfile.GENDER_CHOICES.get_key(
            profile.gender
        )
        gender_subdoc['description'] = profile.get_gender_display()
        # Main document
        document = OrderedDict()
        document['id'] = profile.user.pk
        document['username'] = profile.user.username
        document['name'] = profile.user.get_full_name() \
            if (profile.user.first_name or profile.user.last_name) else None
        document['email'] = profile.user.email
        document['gender'] = gender_subdoc
        #Photo subdoc
        if profile.photo:
            photo_subdoc = OrderedDict()
            document['photo'] = photo_subdoc
            photo_subdoc['name'] = profile.photo.file.name.split('/')[-1]
            photo_subdoc['link'] = profile.get_photo_restful_link_metadata(rel='self')
        else:
            document['photo'] = None
        document['experience_points'] = profile.experience_points
        document['experience_points_week'] = profile.experience_points_week
        document['validated_places'] = profile.num_validated_places
        document['validated_tags'] = profile.num_validated_tags
        document['num_visited_places'] = profile.num_visited_places
        document['num_visited_places_week'] = profile.num_visited_places_week
        document['link'] = profile.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

    @staticmethod
    def short_json_representation(profile, render_json=True):
        # Main document
        document = OrderedDict()
        document['id'] = profile.user.pk
        document['username'] = profile.user.username
        document['link'] = profile.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

    @staticmethod
    def photo_short_json_representation(profile, render_json=True):
        # Main document
        document = OrderedDict()
        document['id'] = profile.user.pk
        document['username'] = profile.user.username
        if profile.photo:
            photo_subdoc = OrderedDict()
            document['photo'] = photo_subdoc
            photo_subdoc['name'] = profile.photo.file.name.split('/')[-1]
            photo_subdoc['link'] = profile.get_photo_restful_link_metadata(rel='self')
        else:
            document['photo'] = None
        document['link'] = profile.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

    @staticmethod
    def timeline_json_representation(visit, render_json=True):
        from places.resources import PlaceResource

        profile = visit.user.profile
        # Main document
        document = OrderedDict()
        document['name'] = profile.user.get_full_name() \
            if (profile.user.first_name or profile.user.last_name) else None
        document['username'] = profile.user.username
        #Visit
        document['place'] = PlaceResource.shorter_json_representation(visit.place,False)
        document['experience_points'] = visit.experience_points
        document['date'] = visit.date
        #Photo subdoc
        if profile.photo:
            photo_subdoc = OrderedDict()
            document['photo'] = photo_subdoc
            photo_subdoc['name'] = profile.photo.file.name.split('/')[-1]
            photo_subdoc['link'] = \
                profile.get_photo_restful_link_metadata(rel='self')
        else:
            document['photo'] = None
        document['link'] = profile.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

    @staticmethod
    def ranking_json_representation(profile, render_json=True):
        # Main document
        document = OrderedDict()
        document['name'] = profile.user.get_full_name() \
            if (profile.user.first_name or profile.user.last_name) else None
        document['username'] = profile.user.username
        document['experience_points_week'] = profile.experience_points_week
        #Photo subdoc
        if profile.photo:
            photo_subdoc = OrderedDict()
            document['photo'] = photo_subdoc
            photo_subdoc['name'] = profile.photo.file.name.split('/')[-1]
            photo_subdoc['link'] = profile.get_photo_restful_link_metadata(rel='self')
        else:
            document['photo'] = None
        document['link'] = profile.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

    @staticmethod
    def access_token_json_representation(access_token, render_json=True):
        document = OrderedDict()
        document['value'] = access_token.uuid
        document['expiration'] = access_token.expiration_date
        if render_json:
            return render_as_json(document)
        else:
            return document

class FriendsRankingCollection(
    RESTfulBasicOrDjangoAuthenticationMixIn,
    CollectionResourceView
):
    """
    Class that gives a ranking of a user's friends ordered by
    the amount of experience points won in a week.
    """

    def get(self, request, *args, **kwargs):

        self.user_id = kwargs.get('user_id', 0)
        try:
            profile = UserProfile.objects.get(user__id=self.user_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = OrderedDict()
            json_document['data']['friends'] = \
                FriendsRankingCollection.json_representation(profile,False)
            json_document['link'] = \
                self.get_restful_link_metadata(rel='self')
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

        except UserProfile.DoesNotExist:
            error = OrderedDict()
            message = unicode(strings.NON_EXISTANT_USER % self.user_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json',
            )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('ranking',kwargs={'user_id':self.user_id})
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.FRIENDS_RANKING)
        metadata['type'] = 'application/json'
        return metadata
        
    @staticmethod
    def json_representation(profile,render_json=True):

        document = OrderedDict()
        friends_list = []
        id = profile.user.id
        friends = UserFriend.objects.raw("""
            SELECT R.id, R.experience_points_week FROM
                (SELECT u.friend_id as id, friend_id as user_id, p.experience_points_week 
                FROM users_userfriend u INNER JOIN users_userprofile p
                ON u.friend_id = p.user_id
                WHERE u.user_id = %s
                UNION 
                SELECT u.user_id as id, u.user_id, p.experience_points_week
                FROM users_userfriend u INNER JOIN users_userprofile p
                ON u.user_id = p.user_id
                WHERE u.friend_id = %s
                UNION
                SELECT user_id as id, user_id, experience_points_week
                FROM users_userprofile
                WHERE user_id = %s)R
                ORDER BY R.experience_points_week DESC;
                """, [id,id,id])
        #friends = UserFriend.objects.select_related().filter(
         #   Q(user=profile.user.id)|
          #  Q(friend=profile.user.id)
        #).order_by('-user__profile__experience_points_week')
        for friend in friends:
            profile = UserProfile.objects.get(user_id=friend.id)
            friends_list.append({"friend":
                UserResource.ranking_json_representation(
                    profile,
                    False
                )}
            )
        return friends_list

class FriendsTimelineCollection(
    RESTfulBasicOrDjangoAuthenticationMixIn,
    CollectionResourceView
):
    """
    Class that gives list of a user's friends visits ordered by
    the date of every visit.
    """

    def get(self, request, *args, **kwargs):

        self.user_id = kwargs.get('user_id', 0)
        try:
            profile = UserProfile.objects.get(user__id=self.user_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = OrderedDict()
            json_document['data']['visits'] = \
                FriendsTimelineCollection.json_representation(profile,False)
            json_document['link'] = \
                self.get_restful_link_metadata(rel='self')
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

        except UserProfile.DoesNotExist:
            error = OrderedDict()
            message = unicode(strings.NON_EXISTANT_USER % self.user_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json',
            )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('timeline',kwargs={'user_id':self.user_id})
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.FRIENDS_TIMELINE)
        metadata['type'] = 'application/json'
        return metadata
        
    @staticmethod
    def json_representation(profile,render_json=True):

        document = OrderedDict()
        visits_list = []
        id = profile.user.id
        visits = UserVisitPlace.objects.raw("""
            SELECT R.id, R.user_id,R.place_id, R.experience_points, R.date FROM
                (SELECT a.id, a.user_id, a.place_id, a.experience_points, a.date 
                FROM users_uservisitplace a INNER JOIN users_userfriend u 
                ON u.user_id= a.user_id 
                WHERE u.friend_id = %s AND a.user_id <> %s
                UNION 
                SELECT a.id, a.user_id, a.place_id, a.experience_points, a.date 
                FROM users_uservisitplace a INNER JOIN users_userfriend u 
                ON u.friend_id = a.user_id 
                WHERE u.user_id = %s AND a.user_id <> %s)R
            ORDER BY R.date DESC LIMIT 50;
            """, [id,id,id,id]
        )
        for visit in visits:
            visits_list.append({"visit":
                UserResource.timeline_json_representation(
                    visit,
                    False
                )}
            )
        return visits_list

class FriendsAfterVisit(
    RESTfulBasicOrDjangoAuthenticationMixIn,
    CollectionResourceView
):
    """
    Class that gives a list of a user's friends activity after
    visiting a place ordered by the date of every activity.
    """

    def get(self, request, *args, **kwargs):

        self.user_id = kwargs.get('user_id', 0)
        self.place_id = kwargs.get('place_id', 0)
        try:
            profile = UserProfile.objects.get(user__id=self.user_id)
            place = Place.objects.get(id=self.place_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = OrderedDict()
            json_document['data']['visits'] = \
                FriendsAfterVisit.json_representation(
                    profile,
                    self.place_id,
                    False)
            json_document['link'] = \
                self.get_restful_link_metadata(rel='self')
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

        except UserProfile.DoesNotExist:
            error = OrderedDict()
            message = unicode(strings.NON_EXISTANT_USER % self.user_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        except Place.DoesNotExist:
            error = OrderedDict()
            message = unicode(strings.NON_EXISTANT_PLACE % self.place_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json',
            )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('aftervisit',kwargs={
                'user_id':self.user_id,
                'place_id':self.place_id
            })
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.FRIENDS_AFTER_VISIT)
        metadata['type'] = 'application/json'
        return metadata
        
    @staticmethod
    def json_representation(profile,place_id,render_json=True):

        document = OrderedDict()
        visits_list = []
        id = profile.user.id
        visits = UserVisitPlace.objects.raw("""
            SELECT R.id, R.user_id,R.place_id, R.experience_points, R.date FROM
                (SELECT a.id, a.user_id, a.place_id, a.experience_points, a.date 
                FROM users_uservisitplace a INNER JOIN users_userfriend u 
                ON u.user_id= a.user_id 
                WHERE u.friend_id = %s AND a.user_id <> %s
                AND a.date > (SELECT date FROM users_uservisitplace v 
                    WHERE user_id = a.user_id AND place_id = %s)
                UNION 
                SELECT a.id, a.user_id, a.place_id, a.experience_points, a.date 
                FROM users_uservisitplace a INNER JOIN users_userfriend u 
                ON u.friend_id = a.user_id 
                WHERE u.user_id = %s AND a.user_id <> %s
                AND a.date > (SELECT v.date FROM users_uservisitplace v 
                    WHERE v.user_id = a.user_id AND v.place_id = %s))R
            ORDER BY R.date DESC;
            """, [id,id,place_id,id,id,place_id]
        )
        for visit in visits:
            visits_list.append({"visit":
                UserResource.timeline_json_representation(
                    visit,
                    False
                )}
            )
        return visits_list