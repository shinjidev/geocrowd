from django.utils.translation import ugettext_lazy as _

TAG_MANDATORY_FIELDS = ['name','user','place']

VALIDATION_MANDATORY_FIELDS = ['vote','user','tag']

TAG_NAME = _(u'Name')
TAG_STATE = _(u'State')
TAG_PLACE = _(u'Place')
TAG_USER = _(u'User')
TAG_YEAH= _(u'Yeah (Positive votes)')
TAG_NAY= _(u'Nay (Negative votes)')
TAG_DATE = _(u'Date')
TAG_VOTE = _(u'Vote')
TAG_VISIBILITY = _(u'Visibility')

TAG_VERBOSE_NAME = _(u'Tag')
TAG_VERBOSE_NAME_PLURAL = _(u'Tags')
TAG_VALIDATION_VERBOSE_NAME = _(u'Tag validation')
TAG_VALIDATION_VERBOSE_NAME_PLURAL = _(u'Tag validations')

STATE_VALIDATED = _(u'Validated')
STATE_TO_BE_VALIDATED = _(u'To be validated')
STATE_UNVALIDATED= _(u'Unvalidated')

NON_EXISTANT_TAG = _(u'Tag %s does not exist!')
TAG_EXISTS = _(u'A tag with that name already exists for this place: %s!')
TAG_COLLECTION = _(u'A collection of all tags.')
INVOLVED_DONT_EXIST = _(u'The %s involved in the tag do(es)n\'t exist!')

NON_EXISTANT_USER = _(u'User %s does not exist!')