from ordereddict import OrderedDict

from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.db.models.fields import BooleanField
from django.conf import settings
from django.core.urlresolvers import reverse

from places.models import Place
from common.datastructures.enumeration import Enumeration
from tags import strings

class Tag(models.Model):

    name=models.CharField(
        verbose_name=strings.TAG_NAME,
        max_length=256,
    )

    STATE_CHOICES = Enumeration([
        (1, 'VALIDATED', strings.STATE_VALIDATED),
        (2, 'TO_BE_VALIDATED', strings.STATE_TO_BE_VALIDATED),
        (3, 'UNVALIDATED', strings.STATE_UNVALIDATED)
    ])

    state=models.PositiveIntegerField(
        verbose_name=strings.TAG_STATE,
        choices=STATE_CHOICES,
        default=2,
    )

    user=models.ForeignKey(User,
        verbose_name=strings.TAG_USER,
    )

    place=models.ForeignKey(Place,
        verbose_name=strings.TAG_PLACE,
    )

    yeah=models.PositiveIntegerField(
        verbose_name=strings.TAG_YEAH,
        editable=False,
        default=0,
    )

    nay=models.PositiveIntegerField(
        verbose_name=strings.TAG_NAY,
        editable=False,
        default=0,
    )

    visibility=BooleanField(
        verbose_name=strings.TAG_VISIBILITY,
        default=True,
    )

    class Meta:
        verbose_name = strings.TAG_VERBOSE_NAME
        verbose_name_plural = strings.TAG_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.name,)

    def get_restful_url(self):
        return '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('tags', kwargs={'tag_id':self.id})
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = self.name
        metadata['type'] = 'application/json'
        return metadata

    @staticmethod
    def create_new_tag(
        name,
        user,
        place
        ):

        new_tag = Tag(
            name=name,
            user=user,
            place=place
        )
        new_tag.save()
        return new_tag

class ValidationTag(models.Model):
    user=models.ForeignKey(User,
        verbose_name=strings.TAG_USER,
    )

    tag=models.ForeignKey(Tag,
        verbose_name=strings.TAG_NAME,
    )

    date = models.DateTimeField(
        verbose_name=strings.TAG_DATE,
        auto_now_add=True,
        editable=False,
    )

    VOTE_CHOICES = Enumeration([
        (1, 'yeah', strings.TAG_YEAH),
        (2, 'nay', strings.TAG_NAY),
    ])

    vote=models.PositiveIntegerField(
        verbose_name=strings.TAG_VOTE,
        choices=VOTE_CHOICES,
        default=1,
    )

    class Meta:
        verbose_name = strings.TAG_VALIDATION_VERBOSE_NAME
        verbose_name_plural = strings.TAG_VALIDATION_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.VOTE_CHOICES.get_key(self.vote),)

    @staticmethod
    def create_new_validation(
        vote,
        user,
        tag
        ):

        vote_value=ValidationTag.VOTE_CHOICES.get_value(vote)
        new_validation = ValidationTag(
            vote=vote_value,
            user=user,
            tag=tag
        )
        new_validation.save()
        return new_validation