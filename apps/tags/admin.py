from django.contrib import admin
from tags.models import Tag, ValidationTag

class TagAdmin(admin.ModelAdmin):
	list_display=(
		'__unicode__',
		'user',
		'place')
	readonly_fields=('yeah','nay',)

admin.site.register(Tag,TagAdmin)

class ValidationTagAdmin(admin.ModelAdmin):
	list_display=(
		'__unicode__',
		'user',
		'tag')
	readonly_fields=('date',)

admin.site.register(ValidationTag,ValidationTagAdmin)