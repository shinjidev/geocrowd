from celery.task import task

from django.conf import settings

from tags.models import Tag, ValidationTag

@task
def validate_tags():
	tags = Tag.objects.filter(state=2)
	for tag in tags:
		yeahs=ValidationTag.objects.filter(tag=tag,vote=1)
		nays=ValidationTag.objects.filter(tag=tag,vote=2)
		yeahs_average=0
		nays_average=0
		for yeah in yeahs:
			yeahs_average += yeah.user.profile.get_kharma_average()
		for nay in nays:
			nays_average += nay.user.profile.get_kharma_average()
		if not tag.yeah == 0: yeahs_average/=tag.yeah
		if not tag.nay == 0: nays_average/=tag.nay
		diff_votes=yeahs_average-nays_average
		if diff_votes >= settings.VALIDATION_POINTS_REQUIRED['tags'] or tag.user.profile.kharma_points>=100:
			tag.state = 1
			tag.visibility = True
			tag.save()
			profile = tag.user.profile
			profile.num_validated_tags+=1
			profile.experience_points+=\
				settings.EXPERIENCE_POINTS_EARNED['own_tag_validated']
			profile.experience_points_week+=\
				settings.EXPERIENCE_POINTS_EARNED['own_tag_validated']
			profile.kharma_points+=\
				settings.KHARMA_POINTS_EARNED['own_tag_validated']
			profile.save()
			for yeah in yeahs:
				profile2 = yeah.user.profile
				profile2.kharma_points+=\
					settings.KHARMA_POINTS_EARNED['voted_tag_validated']
				profile2.save()
			for nay in nays:
				profile2 = nay.user.profile
				profile2.kharma_points-=\
					settings.KHARMA_POINTS_LOST['voted_tag_unvalidated']
		if diff_votes <= (-1*settings.VALIDATION_POINTS_REQUIRED['tags']):
			tag.state = 3
			tag.visibility = False
			tag.save()
			profile = tag.user.profile
			profile.kharma_points-=\
				settings.KHARMA_POINTS_EARNED['own_tag_validated']
			profile.save()
			for yeah in yeahs:
				profile2 = yeah.user.profile
				profile2.kharma_points-=\
					settings.KHARMA_POINTS_LOST['voted_tag_unvalidated']
				profile2.save()
			for nay in nays:
				profile2 = nay.user.profile
				profile2.kharma_points+=\
					settings.KHARMA_POINTS_EARNED['voted_tag_validated']
				profile2.save()

