from django.conf.urls import patterns, include, url

from tags import resources

urlpatterns = patterns('',
	
	url(r'^$',
        resources.TagCollection.as_view(),
        name='tags'),
    url(r'^autocomplete/$',
        resources.TagAutocompleteCollection.as_view(),
        name='autocomplete'),
	url(r'^add/$',
        resources.TagController.as_view(),
        name='add'),
    url(r'^(?P<tag_id>[-\w]+)/vote/$',
        resources.ValidationTagController.as_view(),
        name='vote'),
    url(r'^(?P<tag_id>[-\w]+)/$',
        resources.TagResource.as_view(),
        name='tags'),

)
