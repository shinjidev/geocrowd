import django.utils.simplejson as json

from datetime import datetime
from ordereddict import OrderedDict
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.http import HttpResponseBadRequest
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.models import User

from rest.views import ResourceView
from rest.views import ControllerResourceView
from rest.views import CollectionResourceView

from rest.http import HttpResponseConflict
from rest.auth import RESTfulBasicOrDjangoAuthenticationMixIn as AuthMixIn
from rest.utils import render_as_json
from rest.utils import json_error_document

from users.resources import UserResource
from places.resources import PlaceResource
from places.models import Place
from tags.models import Tag, ValidationTag
from tags import strings

class TagResource(
    AuthMixIn, 
    ResourceView):

    def get(self, request, *args, **kwargs):
        tag_id = kwargs.get('tag_id', 0)
        try:
            tag = Tag.objects.get(id=tag_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                TagResource.json_representation(tag,False)
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )
        except Tag.DoesNotExist:
            error = OrderedDict()
            message = strings.NON_EXISTANT_TAG % (tag_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json'
            )
    
    @staticmethod
    def json_representation(tag, render_json=True):
        # Document
        document = OrderedDict()
        document['id'] = tag.id
        document['name'] = tag.name
        document['state'] = tag.STATE_CHOICES.get_key(
            tag.state
        )
        document['visibility'] = tag.visibility
        document['user'] = UserResource.short_json_representation(tag.user.profile, False)
        document['place'] = PlaceResource.short_json_representation(tag.place, False)
        document['link'] = tag.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document


    @staticmethod
    def short_json_representation(tag, render_json=True):
        document = OrderedDict()
        document['id'] = tag.id
        document['name'] = tag.name
        document['state'] = tag.STATE_CHOICES.get_key(
            tag.state
        )
        document['link'] = tag.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

class TagCollection(CollectionResourceView):

    def get(self, request, *args, **kwargs):
        
        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['data'] = OrderedDict()
        json_document['data']['tags'] = \
            TagCollection.json_representation()
        json_document['link'] = \
            self.get_restful_link_metadata(rel='self')
        return HttpResponse(
            content=render_as_json(json_document),
            content_type='application/json'
        )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('tags')
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.TAG_COLLECTION)
        metadata['type'] = 'application/json'
        print metadata
        return metadata
        
    @staticmethod
    def json_representation(place_id=None,user_id=None,render_json=True):

        if place_id and user_id:
            tags = Tag.objects.filter(place=place_id,user=user_id)
        elif place_id:
            tags = Tag.objects.filter(place=place_id)
        elif user_id:
            tags = Tag.objects.filter(user=user_id)
        else:
            tags = Tag.objects.all()
        tags_list = []
        for tag in tags:
            tags_list.append({"tag":
                TagResource.short_json_representation(
                    tag,
                    False
                )}
            )
        return tags_list

class TagAutocompleteCollection(CollectionResourceView):

    def get(self, request, *args, **kwargs):
        
        data = json.loads(request.raw_post_data)['data']
        search_word = data['search_word']
        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['data'] = OrderedDict()
        json_document['data']['choices'] = \
            TagAutocompleteCollection.json_representation(search_word)
        return HttpResponse(
            content=render_as_json(json_document),
            content_type='application/json'
        )
        
    @staticmethod
    def json_representation(search_word,render_json=True):
        from django.db.models import Count
        tags_list = []
        tags = Tag.objects.filter(
                name__istartswith = search_word
            ).values(
                'name'
            ).annotate(
                count = Count('name')
            ).order_by(
            '-count','name'
        )[0:7]

        for tag in tags:
            tags_list.append(tag['name'])
        return tags_list

class TagController(
    AuthMixIn,
    ControllerResourceView):

    badge_list = []

    def award_badge(self,sender,**kwargs):
        self.badge_list.append(kwargs['badge'].title)

    def run(self, request, *args, **kwargs):
        from badges.signals import badge_awarded
        from users.meta_badges import Veteran
        badge_awarded.connect(self.award_badge, sender=Veteran)

        data = json.loads(request.raw_post_data.decode("latin-1"))
        cleaned_data = data['tag']
        val_keys, entry = self.validate_fields(cleaned_data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        username = cleaned_data['user']['username']
        place_id =cleaned_data['place']['id']
        tag_name = cleaned_data['name']
        missing, user, place = self.valid_user_place(username,place_id) 
        if len(missing) > 0:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.INVOLVED_DONT_EXIST % ",".join(missing))
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json'
            )

        if self.valid_tag(tag_name,place):
            tag = Tag.create_new_tag(
                name=tag_name,
                user=user,
                place=place
            )
            xp_points = settings.EXPERIENCE_POINTS_EARNED['add_tag']
            user.profile.experience_points+=\
                xp_points
            user.profile.experience_points_week+=\
                xp_points
            user.profile.save()

            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                TagResource.short_json_representation(tag,False)
            json_document['badges'] = self.badge_list
            json_document['experience_points'] = xp_points
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

        else:
            error = OrderedDict()
            message = (strings.TAG_EXISTS % place.name)
            error['message'] = unicode(message)
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json'
            )

    def valid_user_place(self,username,place_id):
        missing = []
        val_flag = True
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            missing.append('user')
            user=None
            val_flag = False
        try:
             place =Place.objects.get(id=place_id)
        except Place.DoesNotExist:
            missing.append('place')
            place=None
            val_flag = False
        return missing, user, place


    def valid_tag(self,name,place):
        if Tag.objects.filter(name=name,place=place.id).exists():
            return False
        return True

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.TAG_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif (entry == 'user' and data[entry]['username'] == ""):
                return False, entry
            elif (entry == 'place' and data[entry]['id'] == ""):
                return False, entry
        return True, None

#VALIDATION TAG

class ValidationTagController(
    AuthMixIn,
    ControllerResourceView):

    badge_list = []

    def award_badge(self,sender,**kwargs):
        self.badge_list.append(kwargs['badge'].title)

    def run(self, request, *args, **kwargs):
        from badges.signals import badge_awarded
        from users.meta_badges import Veteran
        badge_awarded.connect(self.award_badge, sender=Veteran)

        data = json.loads(request.raw_post_data)
        val_keys, entry = self.validate_fields(data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        vote=data['vote'].lower()
        print vote
        try:
            tag = Tag.objects.get(
                id=data['tag']['id']
            )
        except Tag.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_TAG % data['tag']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            user = User.objects.get(
                id=data['user']['id']
            )
        except User.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_USER % data['user']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        votes = {"yeah":0,"nay":0}
        vote_value = ValidationTag.VOTE_CHOICES.get_value(vote)          
        try:
            validtag = ValidationTag.objects.get(
                user=user.id,
                tag=tag.id
            )
            xp_points = 0
            if validtag.vote != vote_value:
                validtag.vote = vote_value
                validtag.date = datetime.now()
                votes['yeah'] = votes['nay'] = -1
                votes[vote]=1
                validtag.save()
        except ValidationTag.DoesNotExist:
            validtag = ValidationTag.create_new_validation(
                tag=tag,
                user=user,
                vote=vote
            )
            votes[vote]=1
            xp_points = settings.EXPERIENCE_POINTS_EARNED['validate_tag']
            user.profile.experience_points+=\
                xp_points
            user.profile.experience_points_week+=\
                xp_points
            user.profile.save()

        tag.yeah+=votes['yeah']
        tag.nay+=votes['nay']
        tag.save()

        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['badges'] = self.badge_list
        json_document['experience_points'] = xp_points
        return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.VALIDATION_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif (entry == 'user' or entry == 'tag') and data[entry]['id']=="":
                return False, entry
        return True, None