from django.contrib import admin
from offers.models import Offer, VoteOffer

class OfferAdmin(admin.ModelAdmin):
	list_display=(
		'__unicode__',
		'user',
		'place')
	readonly_fields=('likes','dislikes','date','votes_reached','dislikes_reached')

admin.site.register(Offer,OfferAdmin)

class VoteOfferAdmin(admin.ModelAdmin):
	list_display=(
		'__unicode__',
		'user',
		'offer')
	readonly_fields=('date',)

admin.site.register(VoteOffer,VoteOfferAdmin)