from django.conf.urls import patterns, include, url

from offers import resources

urlpatterns = patterns('',
    url(r'^add/$',
        resources.OfferController.as_view(),
        name='add'),
    url(r'^(?P<offer_id>[-\w]+)/like/$',
        resources.VoteOfferController.as_view(),
        name='like'),
    url(r'^$',
        resources.OfferCollection.as_view(),
        name='offers'),
	url(r'^(?P<offer_id>[-\w]+)/$',
        resources.OfferResource.as_view(),
        name='offers')
)