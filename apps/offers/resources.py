import django.utils.simplejson as json

from datetime import datetime
from ordereddict import OrderedDict
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.http import HttpResponseBadRequest
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.models import User

from rest.views import ResourceView
from rest.views import ControllerResourceView
from rest.views import CollectionResourceView

from rest.http import HttpResponseConflict
from rest.http import HttpResponseNoContent
from rest.auth import RESTfulBasicOrDjangoAuthenticationMixIn as AuthMixIn
from rest.utils import render_as_json
from rest.utils import json_error_document

from users.resources import UserResource
from places.resources import PlaceResource
from places.models import Place
from offers.models import Offer, VoteOffer
from offers import strings

class OfferResource(
    AuthMixIn, 
    ResourceView):

    def get(self, request, *args, **kwargs):
        offer_id = kwargs.get('offer_id', 0)
        try:
            offer = Offer.objects.get(id=offer_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                OfferResource.json_representation(offer,False)
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )
        except Offer.DoesNotExist:
            error = OrderedDict()
            message = strings.NON_EXISTANT_OFFER % (offer_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json'
            )

    @staticmethod
    def json_representation(offer, render_json=True):
        # Document
        document = OrderedDict()
        document['id'] = offer.id
        document['description'] = offer.description
        document['date'] = offer.date 
        document['visibility'] = offer.visibility
        document['likes'] = offer.likes
        document['dislikes'] = offer.dislikes
        document['user'] = UserResource.short_json_representation(offer.user.profile, False)
        document['place'] = PlaceResource.short_json_representation(offer.place, False)
        document['link'] = offer.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document


    @staticmethod
    def short_json_representation(offer, render_json=True):
        document = OrderedDict()
        document['id'] = offer.id
        document['description'] = offer.description
        document['likes'] = offer.likes
        document['dislikes'] = offer.dislikes
        document['link'] = offer.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

class OfferCollection(CollectionResourceView):

    def get(self, request, *args, **kwargs):
        
        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['data'] = OrderedDict()
        json_document['data']['offers'] = \
            OfferCollection.json_representation()
        json_document['link'] = \
            self.get_restful_link_metadata(rel='self')
        print json_document['link']
        return HttpResponse(
            content=render_as_json(json_document),
            content_type='application/json'
        )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('offers')
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.OFFER_COLLECTION)
        metadata['type'] = 'application/json'
        print metadata
        return metadata
        
    @staticmethod
    def json_representation(place_id=None,user_id=None,render_json=True):

        if place_id and user_id:
            offers = Offer.objects.filter(place=place_id,user=user_id)
        elif place_id:
            offers = Offer.objects.filter(place=place_id)
        elif user_id:
            offers = Offer.objects.filter(user=user_id)
        else:
            offers = Offer.objects.all()
        offers_list = []
        for offer in offers:
            offers_list.append({"offer":
                OfferResource.short_json_representation(
                    offer,
                    False
                )}
            )
        return offers_list

class OfferController(
    AuthMixIn,
    ControllerResourceView):

    badge_list = []

    def award_badge(self,sender,**kwargs):
        from users.badge_resources.resources import BadgeResource
        self.badge_list.append(BadgeResource.short_json_representation(
            kwargs['badge'],False)
        )

    def run(self, request, *args, **kwargs):
        from badges.signals import badge_awarded
        from users.meta_badges import Salesman, Veteran
        badge_awarded.connect(self.award_badge, sender=Salesman)
        badge_awarded.connect(self.award_badge, sender=Veteran)

        data = json.loads(request.raw_post_data.decode("latin-1"))
        cleaned_data = data['offer']
        val_keys, entry = self.validate_fields(cleaned_data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        username = cleaned_data['user']['username']
        place_id =cleaned_data['place']['id']
        offer_description = cleaned_data['description']
        missing, user, place = self.valid_user_place(username,place_id) 
        if len(missing) > 0:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.INVOLVED_DONT_EXIST % ",".join(missing))
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json'
            )

        if self.valid_offer(offer_description,place):
            offer = Offer.create_new_offer(
                description=offer_description,
                user=user,
                place=place
            )
            xp_points = settings.EXPERIENCE_POINTS_EARNED['add_offer']
            user.profile.experience_points+=\
                xp_points
            user.profile.experience_points_week+=\
                xp_points
            user.profile.save()

            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                OfferResource.short_json_representation(offer,False)
            json_document['badges'] = self.badge_list
            json_document['experience_points'] = xp_points
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )
        else:
            error = OrderedDict()
            message = (strings.OFFER_EXISTS % place.name)
            error['message'] = unicode(message)
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json'
            )

    def valid_user_place(self,username,place_id):
        missing = []
        val_flag = True
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            missing.append('user')
            user=None
            val_flag = False
        try:
             place =Place.objects.get(id=place_id)
        except Place.DoesNotExist:
            missing.append('place')
            place=None
            val_flag = False
        return missing, user, place


    def valid_offer(self,description,place):
        if Offer.objects.filter(description=description,place=place.id).exists():
            return False
        return True

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.OFFER_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif (entry == 'user' and data[entry]['username'] == ""):
                return False, entry
            elif (entry == 'place' and data[entry]['id'] == ""):
                return False, entry
        return True, None

class VoteOfferController(
    AuthMixIn,
    ControllerResourceView):

    def run(self, request, *args, **kwargs):
        data = json.loads(request.raw_post_data)
        val_keys, entry = self.validate_fields(data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        vote=data['like'].lower()
        try:
            offer = Offer.objects.get(
                id=data['offer']['id']
            )
        except Offer.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_OFFER % data['offer']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            user = User.objects.get(
                id=data['user']['id']
            )
        except User.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_USER % data['user']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        likes = {"like":0,"dislike":0}
        vote_value = VoteOffer.VOTE_CHOICES.get_value(vote)

        try:
            voteoffer = VoteOffer.objects.get(
                user=user.id,
                offer=offer.id
            )
            if voteoffer.vote != vote_value:
                voteoffer.vote = vote_value
                voteoffer.date = datetime.now()
                likes['like'] = likes['dislike'] = -1
                likes[vote]=1
                voteoffer.save()
        except VoteOffer.DoesNotExist:
            voteoffer = VoteOffer.create_new_vote(
                offer=offer,
                user=user,
                vote=vote,
            )
            likes[vote] = 1

        offer.likes+=likes['like']
        offer.dislikes+=likes['dislike']
        offer.save()
        if not offer.votes_reached or not offer.dislikes_reached:
            self.check_kharma_points(offer)
        return HttpResponseNoContent()

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.VOTE_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif (entry == 'user' or entry == 'offer') and data[entry]['id']=="":
                return False, entry
        return True, None

    def check_kharma_points(self,offer):
        if not offer.votes_reached:
            votes = VoteOffer.objects.filter(offer=offer)
            lk_kharma = 0
            num_lk = 0.0
            dlk_kharma = 0
            num_dlk = 0.0
            for vote in votes:
                if vote.vote == 1:
                    lk_kharma+=vote.user.profile.get_kharma_average()
                    num_lk+=1
                if vote.vote == 2:
                    dlk_kharma+=vote.user.profile.get_kharma_average()
                    num_dlk+=1        
            if not num_lk==0:
                lk_kharma/=num_lk
            if not num_dlk==0:
                dlk_kharma/=num_dlk
            if (lk_kharma-dlk_kharma) > settings.VALIDATION_POINTS_REQUIRED['offers']:
                offer.votes_reached = True
                offer.dislikes_reached = False
                offer.save()
                profile = offer.user.profile
                profile.kharma_points += settings.KHARMA_POINTS_EARNED['own_offer_voted']
                profile.save()
                for vote in votes:
                    profile2 = vote.user.profile
                    if vote.vote == 1:
                        profile2.kharma_points+=settings.KHARMA_POINTS_EARNED['voted_offer_validated']
                    if vote.vote == 2:
                        profile2.kharma_points-=settings.KHARMA_POINTS_LOST['voted_offer_unvalidated']
                    profile2.save()

        if not offer.dislikes_reached:
            votes = VoteOffer.objects.filter(offer=offer)
            lk_kharma = 0
            num_lk = 0.0
            dlk_kharma = 0
            num_dlk = 0.0
            for vote in votes:
                if vote.vote == 1:
                    lk_kharma+=vote.user.profile.get_kharma_average()
                    num_lk+=1
                if vote.vote == 2:
                    dlk_kharma+=vote.user.profile.get_kharma_average()
                    num_dlk+=1
            if not num_lk==0:
                lk_kharma/=num_lk
            if not num_dlk==0:
                dlk_kharma/=num_dlk
            if (dlk_kharma-lk_kharma) > settings.VALIDATION_POINTS_REQUIRED['offers']:
                offer.votes_reached = False
                offer.dislikes_reached = True
                offer.save()
                profile = offer.user.profile
                profile.kharma_points -= settings.KHARMA_POINTS_LOST['own_offer_disliked']
                profile.save()
                for vote in votes:
                    profile2 = vote.user.profile
                    if vote.vote == 1:
                        profile2.kharma_points-=settings.KHARMA_POINTS_LOST['voted_offer_unvalidated']
                    if vote.vote == 2:
                        profile2.kharma_points+=settings.KHARMA_POINTS_EARNED['voted_offer_validated']
                    profile2.save()