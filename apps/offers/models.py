from ordereddict import OrderedDict

from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.db.models.fields import BooleanField
from django.conf import settings
from django.core.urlresolvers import reverse

from places.models import Place
from offers import strings

class Offer(models.Model):

    description=models.TextField(
        verbose_name=strings.OFFER_DESCRIPTION,
    )

    user=models.ForeignKey(User,
        verbose_name=strings.OFFER_USER,
    )

    place=models.ForeignKey(Place,
        verbose_name=strings.OFFER_PLACE,
    )

    likes = models.PositiveIntegerField(
        verbose_name=strings.OFFER_LIKE,
        editable=False,
        default=0,
    )

    dislikes = models.PositiveIntegerField(
        verbose_name=strings.OFFER_DISLIKE,
        editable=False,
        default=0,
    )

    date = models.DateTimeField(
    	verbose_name=strings.OFFER_DATE,
		auto_now_add=True,
        editable=False
    )

    votes_reached=BooleanField(
        verbose_name=strings.OFFER_VOTES_REACHED,
        default=False,
    )

    dislikes_reached=BooleanField(
        verbose_name=strings.OFFER_DISLIKES_REACHED,
        default=False,
    )

    visibility=BooleanField(
        verbose_name=strings.OFFER_VISIBILITY,
        default=True,
    )

    class Meta:
        verbose_name = strings.OFFER_VERBOSE_NAME
        verbose_name_plural = strings.OFFER_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.description,)

    def get_likes_difference(self):
        return (self.likes - self.dislikes)

    def get_restful_url(self):
        return '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('offers', kwargs={'offer_id':self.id})
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = self.description
        metadata['type'] = 'application/json'
        return metadata

    @staticmethod
    def create_new_offer(
        description,
        user,
        place
        ):

        new_offer = Offer(
            description=description,
            user=user,
            place=place
        )
        new_offer.save()
        return new_offer

from common.datastructures.enumeration import Enumeration

class VoteOffer(models.Model):
    user=models.ForeignKey(User,
        verbose_name=strings.OFFER_USER,
    )

    offer=models.ForeignKey(Offer,
        verbose_name=strings.OFFER_VERBOSE_NAME,
    )

    date = models.DateTimeField(
        verbose_name=strings.OFFER_DATE,
        auto_now_add=True,
        editable=False,
    )

    VOTE_CHOICES = Enumeration([
        (1, 'like', strings.OFFER_LIKE),
        (2, 'dislike', strings.OFFER_DISLIKE),
    ])

    vote=models.PositiveIntegerField(
        verbose_name=strings.OFFER_VOTE,
        choices=VOTE_CHOICES,
        default=1,
    )

    class Meta:
        verbose_name = strings.OFFER_VOTE_VERBOSE_NAME
        verbose_name_plural = strings.OFFER_VOTE_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.VOTE_CHOICES.get_key(self.vote),)

    @staticmethod
    def create_new_vote(
        vote,
        user,
        offer
        ):

        vote_value=VoteOffer.VOTE_CHOICES.get_value(vote)
        new_vote = VoteOffer(
            vote=vote_value,
            user=user,
            offer=offer
        )
        new_vote.save()
        return new_vote