from django.utils.translation import ugettext_lazy as _

OFFER_MANDATORY_FIELDS = ['description','user','place']

VOTE_MANDATORY_FIELDS = ['like','user','offer']

OFFER_DESCRIPTION = _(u'Description')
OFFER_PLACE = _(u'Place')
OFFER_USER = _(u'User')
OFFER_LIKE = _(u'Likes')
OFFER_DISLIKE = _(u'Dislikes')
OFFER_VISIBILITY = _(u'Visibility')
OFFER_DATE = _(u'Date')
OFFER_VOTE = _(u'Vote')
OFFER_VOTES_REACHED = _(u'Likes Reached')
OFFER_DISLIKES_REACHED = _(u'Dislikes Reached')

OFFER_VERBOSE_NAME = _(u'Offer')
OFFER_VERBOSE_NAME_PLURAL = _(u'Offers')
OFFER_VOTE_VERBOSE_NAME = _(u'Offer vote')
OFFER_VOTE_VERBOSE_NAME_PLURAL = _(u'Offer votes')

NON_EXISTANT_OFFER = _(u'Offer %s does not exist!')
OFFER_EXISTS = _(u'An offer with that name already exists for this place: %s!')
OFFER_COLLECTION = _(u'A collection of all offers.')
INVOLVED_DONT_EXIST = _(u'The %s involved in the offer do(es)n\'t exist!')

NON_EXISTANT_USER = _(u'User %s does not exist!')