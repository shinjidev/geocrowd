import datetime

class GMTm5(datetime.tzinfo):
    def utcoffset(self, dt):
        return datetime.timedelta(hours=-5)
    def dst(self, dt):
        return datetime.timedelta(0)
    def tzname(self,dt):
        return "Lima/Peru"
    
    @staticmethod
    def now():
    	def now_func():
    		return datetime.datetime.now(GMTm5())
    	return now_func

def datetime_plus_days(days=0):
    def datetime_plus():
        return (
            datetime.datetime.now(GMTm5())+
            datetime.timedelta(days=days)
        )
    return datetime_plus