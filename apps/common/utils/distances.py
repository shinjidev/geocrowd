import math

def point_from_distance(lat,lon,bearing,distance):
    R = 6371.0
    distance = distance/R
    bearing = math.radians(bearing)
    lat1 = math.radians(lat)
    lon1 = math.radians(lon)
    lat2 = math.asin(
        math.sin(lat1)*math.cos(distance) + 
        math.cos(lat1)*math.sin(distance)*math.cos(bearing)
    )
    lon2 = lon1 + math.atan2(
        math.sin(bearing)*math.sin(distance)*math.cos(lat1),
        math.cos(distance)-math.sin(lat1)*math.sin(lat2)
    )
    lon2 = (lon2+3*math.pi) % (2*math.pi) - math.pi    #normalise to -180..+180
    return [math.degrees(lat2),math.degrees(lon2)]