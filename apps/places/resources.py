import django.utils.simplejson as json

from datetime import datetime
from ordereddict import OrderedDict
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.http import HttpResponseBadRequest
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.models import User

from rest.views import ResourceView
from rest.views import ControllerResourceView
from rest.views import CollectionResourceView

from rest.http import HttpResponseConflict
from rest.http import HttpResponseNoContent
from rest.auth import RESTfulBasicOrDjangoAuthenticationMixIn as AuthMixIn
from rest.utils import render_as_json
from rest.utils import json_error_document

from places import models
from places import strings

class PlaceResource(
    AuthMixIn, 
    ResourceView):

    def get(self, request, *args, **kwargs):
        place_id = kwargs.get('place_id', 0)
        data = json.loads(request.raw_post_data)
        data = data['user']
        user_id = data['id']
        try:
            user = User.objects.get(id=user_id)
            place = models.Place.objects.get(id=place_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                PlaceResource.json_representation(place,user,False)
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )
        except models.Place.DoesNotExist:
            error = OrderedDict()
            message = strings.NON_EXISTANT_PLACE % (place_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json'
            )
        except User.DoesNotExist:
            error = OrderedDict()
            message = strings.NON_EXISTANT_USER % (user_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json'
            )
    
    @staticmethod
    def json_representation(place, user, render_json=True):

        #Avoid circular imports
        from tags.resources import TagCollection
        from comment.resources import CommentCollection
        from offers.resources import OfferCollection
        from users.resources import UserResource
        from users.checkin.resources import UserVisitPlaceController
        # Document
        document = OrderedDict()
        document['id'] = place.id
        document['name'] = place.name
        document['user'] = \
            UserResource.short_json_representation(place.user.profile, False)
        document['description'] = \
            place.description if place.description else None
        document['position'] = place.get_coordinates()
        document['state'] = models.Place.STATE_CHOICES.get_key(
            place.state
        )
        document['type'] = \
            place.place_type.get_json_representation(False)
        document['address'] = \
            place.address if place.address else None
        document['country'] = \
            unicode(place.country.name) if place.country else None
        document['likes'] = place.likes
        document['dislikes'] = place.dislikes
        document['visibility'] = place.visibility
        #Photo subdoc
        if place.photo:
            photo_subdoc = OrderedDict()
            document['photo'] = photo_subdoc
            photo_subdoc['name'] = place.photo.file.name.split('/')[-1]
            photo_subdoc['link'] = \
                place.get_photo_restful_link_metadata(rel='self')
        else:
            document['photo'] = None
        #Tags subdoc
        document['tags'] = TagCollection.json_representation(
            place_id=place.id,
            render_json=False
        )
        #Comments subdoc
        document['comments'] = CommentCollection.json_representation(
            place_id=place.id,
            render_json=False
        )
        #Offers subdoc
        document['offers'] = OfferCollection.json_representation(
            place_id=place.id,
            render_json=False
        )
        #Modifications subdoc
        document['modifications'] = \
            ModificationPlaceCollection.json_representation(
                place_id=place.id,
                render_json=False
            )
        document['visited_today'] = \
            UserVisitPlaceController.visited_today(place,user)
        document['link'] = place.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document


    @staticmethod
    def short_json_representation(place, render_json=True):
        from tags.resources import TagCollection
        document = OrderedDict()
        document['id'] = place.id
        document['name'] = place.name
        document['position'] = place.get_coordinates()
        document['state'] = models.Place.STATE_CHOICES.get_key(
            place.state
        )
        document['type'] = \
            place.place_type.get_json_representation(False)
        document['address'] = \
            place.address if place.address else None
        document['country'] = \
            unicode(place.country.name) if place.country else None
        #Tags subdoc
        document['tags'] = TagCollection.json_representation(
            place_id=place.id,
            render_json=False
        )
        document['link'] = place.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

    @staticmethod
    def shorter_json_representation(place, render_json=True):
        document = OrderedDict()
        document['id'] = place.id
        document['name'] = place.name
        document['link'] = place.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

class PlaceCollection(CollectionResourceView):

    def get(self, request, *args, **kwargs):
        
        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['data'] = json.loads(
            PlaceCollection.json_representation()
        )
        json_document['link'] = \
            self.get_restful_link_metadata(rel='self')
        return HttpResponse(
            content=render_as_json(json_document),
            content_type='application/json'
        )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('places')
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.PLACE_COLLECTION)
        metadata['type'] = 'application/json'
        return metadata
        
    @staticmethod
    def json_representation(render_json=True):

        document = OrderedDict()
        places_list = []
        places = models.Place.objects.all()
        for place in places:
            places_list.append({"place":
                PlaceResource.short_json_representation(
                    place,
                    False
                )}
            )
        document['places'] = places_list
        return render_as_json(document)

class NearPlacesCollection(CollectionResourceView):

    def get(self, request, *args, **kwargs):
        data = json.loads(request.raw_post_data)['data']
        latitude = data['position']['latitude']
        longitude = data['position']['longitude']
        distance = data['distance']
        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['data'] = json.loads(
            NearPlacesCollection.json_representation(
                float(longitude),
                float(latitude),
                float(distance)
            )
        )
        json_document['link'] = \
            self.get_restful_link_metadata(rel='self')
        return HttpResponse(
            content=render_as_json(json_document),
            content_type='application/json'
        )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('nearest')
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.NEAR_PLACES_COLLECTION)
        metadata['type'] = 'application/json'
        return metadata
        
    @staticmethod
    def json_representation(longitude,latitude,distance,render_json=True):
        from django.contrib.gis.measure import D
        from django.contrib.gis.geos import Point
        #NorthEastBorderVertex
        #point1 = point_from_distance(latitude,longitude,45,distance)
        #SouthWestBorderVertex
        #point2 = point_from_distance(latitude,longitude,225,distance)
        point = Point(longitude,latitude)
        document = OrderedDict()
        places_list = []
        places = \
            models.Place.objects.filter(
                coordinates__distance_lt=(point,D(km=distance))
                ).distance(point).order_by('distance')
        for place in places:
            #if (point2[0] < place.coordinates.y < point1[0]) and \
                #(point2[1] < place.coordinates.x < point1[1]):
                places_list.append({"place":
                    PlaceResource.short_json_representation(
                        place,
                        False
                    )}
                )
        document['places'] = places_list
        return render_as_json(document)
        
class PlaceController(
    AuthMixIn,
    ControllerResourceView):

    badge_list = []

    def award_badge(self,sender,**kwargs):
        from users.badge_resources.resources import BadgeResource
        self.badge_list.append(BadgeResource.short_json_representation(
            kwargs['badge'],False)
        )

    def run(self, request, *args, **kwargs):
        from badges.signals import badge_awarded
        from users.meta_badges import Veteran
        badge_awarded.connect(self.award_badge, sender=Veteran)

        data = json.loads(request.raw_post_data.decode("latin-1"))
        cleaned_data = data['place']
        try:
            tags_data = data['tags']
        except:
            tags_data = []
        val_keys, entry = self.validate_fields(cleaned_data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            user = User.objects.get(
                id=cleaned_data['user']['id']
            )
        except User.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_USER % cleaned_data['user']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        longitude = cleaned_data['position']['longitude']
        latitude = cleaned_data['position']['latitude']
        coordinates = "".join([
            'POINT (',
            longitude, ' ',
            latitude, ')'
        ])
        address = cleaned_data['address']

        if self.valid_place(cleaned_data['name'],coordinates,address):
            place_type=cleaned_data['type']['description']
            try:
                place_type = models.PlaceType.objects.get(
                    description=place_type
                )
                if user.profile.kharma_points >= 100:
                    visibility=True
                    state=1
                else:
                    visibility=False
                    state=2
                place = models.Place.create_new_place(
                    name=cleaned_data['name'],
                    user=user,
                    description=cleaned_data['description'],
                    coordinates=coordinates,
                    address=address,
                    country=cleaned_data['country'],
                    place_type=place_type,
                    visibility=visibility,
                    state=state,
                    photo=None
                )
                extra_xp_points = 0
                if tags_data:
                    num_tags=self.save_tags(tags_data,place,user)
                    extra_xp_points+=\
                        settings.EXPERIENCE_POINTS_EARNED['add_tag']*num_tags

                xp_points = settings.EXPERIENCE_POINTS_EARNED['add_place']
                user.profile.experience_points+=\
                    xp_points+extra_xp_points
                user.profile.experience_points_week+=\
                    xp_points+extra_xp_points
                user.profile.save()

                json_document = OrderedDict()
                json_document['status'] = 'success'
                json_document['data'] = \
                    PlaceResource.short_json_representation(place,False)
                json_document['badges'] = self.badge_list
                json_document['experience_points'] = xp_points
                return HttpResponse(
                    content=render_as_json(json_document),
                    content_type='application/json',
                )
            except models.PlaceType.DoesNotExist:
                error = OrderedDict()
                error['message'] = unicode(strings.PLACE_TYPE_DOESNT_EXISTS)
                error_list = [error]
                return HttpResponseConflict(
                    content=json_error_document(error_list),
                    content_type='application/json',
                )
        else:
            error = OrderedDict()
            error['message'] = unicode(strings.PLACE_EXISTS)
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json'
            )

    def save_tags(self,data,place, user):
        from tags.models import Tag
        for tag in data:
            Tag.create_new_tag(
                place=place,
                user=user,
                name=tag['name']
            )
        return len(data)


    def valid_place(self,name,coordinates,address):
        if models.Place.objects.filter(
            name=name,
            coordinates=coordinates,
            address=address).exists():
            return False
        return True

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.PLACE_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif entry == 'user' and data[entry]['id'] == "":
                return False, entry
            elif (entry == 'position' and( 
                data[entry]['longitude'] == "" or
                data[entry]['latitude'] == ""
                )):
                return False, entry
        return True, None

class PhotoPlaceController(
    AuthMixIn,
    ControllerResourceView):
    
    def run(self, request, *args, **kwargs):
        place_id = kwargs.get('place_id', 0)

        try:
            photo = request.FILES['photo']
        except:
            photo = None
        try:
            place = models.Place.objects.get(id=place_id)
            place.photo = photo
            place.save()
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                PlaceResource.short_json_representation(place,False)
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )
        except models.Place.DoesNotExist:
            error = OrderedDict()
            message = strings.NON_EXISTANT_PLACE % (place_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json'
            )

#VOTE PLACE

class VotePlaceController(
    AuthMixIn,
    ControllerResourceView):

    def run(self, request, *args, **kwargs):
        data = json.loads(request.raw_post_data)
        val_keys, entry = self.validate_fields(data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        vote=data['like'].lower()
        try:
            place = models.Place.objects.get(
                id=data['place']['id']
            )
        except models.Place.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_PLACE % data['place']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            user = User.objects.get(
                id=data['user']['id']
            )
        except User.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_USER % data['user']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        likes = {"like":0,"dislike":0}
        vote_value = models.VotePlace.VOTE_CHOICES.get_value(vote)
        try:
            voteplace = models.VotePlace.objects.get(
                user=user.id,
                place=place.id
            )
            if voteplace.vote != vote_value:
                voteplace.vote = vote_value
                voteplace.date = datetime.now()
                likes['like'] = likes['dislike'] = -1
                likes[vote]=1
                voteplace.save()
        except models.VotePlace.DoesNotExist:
            voteplace = models.VotePlace.create_new_vote(
                place=place,
                user=user,
                vote=vote
            )
            likes[vote] = 1
        place.likes+=likes['like']
        place.dislikes+=likes['dislike']
        place.save()
        return HttpResponseNoContent()

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.VOTE_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif (entry == 'user' or entry == 'place') and data[entry]['id']=="":
                return False, entry
        return True, None


#VALIDATION PLACE

class ValidationPlaceController(
    AuthMixIn,
    ControllerResourceView):

    badge_list = []

    def award_badge(self,sender,**kwargs):
        from users.badge_resources.resources import BadgeResource
        self.badge_list.append(BadgeResource.short_json_representation(
            kwargs['badge'],False)
        )

    def run(self, request, *args, **kwargs):
        from badges.signals import badge_awarded
        from users.meta_badges import Veteran
        badge_awarded.connect(self.award_badge, sender=Veteran)

        data = json.loads(request.raw_post_data)
        val_keys, entry = self.validate_fields(data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        vote=data['vote'].lower()
        try:
            place = models.Place.objects.get(
                id=data['place']['id']
            )
        except models.Place.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_PLACE % data['place']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            user = User.objects.get(
                id=data['user']['id']
            )
        except User.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_USER % data['user']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            validplace = models.ValidationPlace.objects.get(
                user=user.id,
                place=place.id
            )
            xp_points = 0
            validplace.vote = \
                models.ValidationPlace.VOTE_CHOICES.get_value(vote)
            validplace.date = datetime.now()
            validplace.save()
        except models.ValidationPlace.DoesNotExist:
            validplace = models.ValidationPlace.create_new_validation(
                place=place,
                user=user,
                vote=vote
            )
            xp_points = settings.EXPERIENCE_POINTS_EARNED['validate_place']
            user.profile.experience_points+=\
                xp_points
            user.profile.experience_points_week+=\
                xp_points
            user.profile.save()

        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['badges'] = self.badge_list
        json_document['experience_points'] = xp_points
        return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.VALIDATION_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif (entry == 'user' or entry == 'place') and data[entry]['id']=="":
                return False, entry
        return True, None

#MODIFICATION PLACE

class ModificationPlaceResource(
    AuthMixIn, 
    ResourceView):
    """Class that implements a modification place as a 
    Rest resource. For convenience the name modification place
    will be shortened to modplace"""

    def get(self, request, *args, **kwargs):
        mod_id = kwargs.get('mod_id', 0)
        try:
            modplace = models.ModificationPlace.objects.get(id=mod_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                ModificationPlaceResource.json_representation(
                    modplace,
                    False
                )
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )
        except models.ModificationPlace.DoesNotExist:
            error = OrderedDict()
            message = strings.NON_EXISTANT_MODIFICATION % (mod_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json'
            )
    
    @staticmethod
    def json_representation(modplace, render_json=True):
        from users.resources import UserResource
        # Document
        document = OrderedDict()
        document['id'] = modplace.id
        document['modification_type'] = \
            models.ModificationPlace.TYPE_CHOICES.get_key(
                modplace.modification_type
            )
        document['name'] = \
            modplace.name if modplace.name else None
        document['description'] = \
            modplace.description if modplace.description else None
        document['position'] = modplace.get_coordinates()
        document['type'] = \
            modplace.place_type.get_json_representation(
                False
                ) if modplace.place_type else None
        document['address'] = \
            modplace.address if modplace.address else None
        document['country'] = \
            unicode(modplace.country.name) if modplace.country else None
        #Photo subdoc
        if modplace.photo:
            photo_subdoc = OrderedDict()
            document['photo'] = photo_subdoc
            photo_subdoc['name'] = modplace.photo.file.name.split('/')[-1]
            photo_subdoc['link'] = \
                modplace.get_photo_restful_link_metadata(rel='self')
        else:
            document['photo'] = None
        document['place'] = \
            PlaceResource.short_json_representation(modplace.place,False)
        document['user'] = \
            UserResource.short_json_representation(modplace.user.profile, False)
        document['link'] = modplace.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document


    @staticmethod
    def short_json_representation(modplace, render_json=True):
        document = OrderedDict()
        document['id'] = modplace.id
        document['modification_type'] = \
            models.ModificationPlace.TYPE_CHOICES.get_key(
                modplace.modification_type
            )
        document['name'] = \
            modplace.name if modplace.name else None
        document['description'] = \
            modplace.description if modplace.description else None
        document['position'] = modplace.get_coordinates() if modplace.get_coordinates() else None
        document['link'] = modplace.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

class ModificationPlaceCollection(CollectionResourceView):

    def get(self, request, *args, **kwargs):

        try:
            place_id = kwargs.get('place_id', 0)
        except:
            place_id = None
        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['data'] = OrderedDict()
        json_document['data']['modifications'] = \
            ModificationPlaceCollection.json_representation(place_id)
        json_document['link'] = \
            self.get_restful_link_metadata(rel='self')
        return HttpResponse(
            content=render_as_json(json_document),
            content_type='application/json'
        )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('modifications')
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.MODIFICATIONS_COLLECTION)
        metadata['type'] = 'application/json'
        return metadata
        
    @staticmethod
    def json_representation(place_id=None,render_json=True):
        if place_id:
            modifications = models.ModificationPlace.objects.filter(place=place_id)
        else:
            modifications = models.ModificationPlace.objects.all()

        modifications_list = []
        for modplace in modifications:
            modifications_list.append({"modification":
                ModificationPlaceResource.short_json_representation(
                    modplace,
                    False
                )}
            )
        return modifications_list

class ModificationPlaceController(
    AuthMixIn,
    ControllerResourceView):

    badge_list = []

    def award_badge(self,sender,**kwargs):
        from users.badge_resources.resources import BadgeResource
        self.badge_list.append(BadgeResource.short_json_representation(
            kwargs['badge'],False)
        )

    def run(self, request, *args, **kwargs):
        from badges.signals import badge_awarded
        from users.meta_badges import Veteran
        badge_awarded.connect(self.award_badge, sender=Veteran)

        data = json.loads(request.raw_post_data)
        cleaned_data = data['modification']
        val_keys, entry = self.validate_fields(cleaned_data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        try:
            user = User.objects.get(
                id=cleaned_data['user']['id']
            )
        except User.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_USER % cleaned_data['user']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        try:
            place = models.Place.objects.get(
                id=cleaned_data['place']['id']
            )
        except models.Place.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_PLACE % cleaned_data['place']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        if cleaned_data['modification_type'] == "Modification":
            if cleaned_data['type']:
                place_type=cleaned_data['type']['description']
                try:
                    place_type = models.PlaceType.objects.get(
                        description=place_type
                    )
                except models.PlaceType.DoesNotExist:
                    place_type=place.place_type
            else:
                place_type=place.place_type
            if cleaned_data['position']:
                longitude = cleaned_data['position']['longitude']
                latitude = cleaned_data['position']['latitude']
                coordinates = "".join([
                    'POINT (',
                    longitude, ' ',
                    latitude, ')'
                ])
            else:
                coordinates=place.coordinates
            name=unicode(cleaned_data['name']) if cleaned_data['name'] else place.name 
            description= \
                unicode(cleaned_data['description']) if cleaned_data['description'] else place.description
            address= \
                unicode(cleaned_data['address']) if cleaned_data['address'] else place.address
            country= \
                unicode(cleaned_data['country']) if cleaned_data['country'] else place.country
            if models.ModificationPlace.objects.filter(
                modification_type=1,
                place=place,
                name=name,
                description=description,
                coordinates=coordinates,
                address=address,
                country=country,
                place_type=place_type,
                ).exists():
                error = OrderedDict()
                error['message'] = unicode(strings.MODIFICATION_EXISTS)
                error_list = [error]
                return HttpResponseConflict(
                    content=json_error_document(error_list),
                    content_type='application/json'
                )
            modplace = models.ModificationPlace.create_new_modification(
                modification_type=1,
                place=place,
                user=user,
                name=name,
                description=description,
                coordinates=coordinates,
                address=address,
                country=country,
                place_type=place_type,
            )
            subdoc = \
                ModificationPlaceResource.short_json_representation(
                    modplace,
                    False
                )
        else:
            description = cleaned_data['description']
            if models.ModificationPlace.objects.filter(
                modification_type=2,
                place=place,
                user=user,
                description=description,
                ).exists():
                error = OrderedDict()
                error['message'] = unicode(strings.UNEXISTANT_PLACE_EXISTS)
                error_list = [error]
                return HttpResponseConflict(
                    content=json_error_document(error_list),
                    content_type='application/json'
                )
            unexistant = models.ModificationPlace.create_new_modification(
                modification_type=2,
                place=place,
                user=user,
                name=place.name,
                description=description
            )
            subdoc = \
                ModificationPlaceResource.short_json_representation(
                    unexistant,
                    False
                )

        xp_points = settings.EXPERIENCE_POINTS_EARNED['add_modification']
        user.profile.experience_points+=\
            xp_points
        user.profile.experience_points_week+=\
            xp_points
        user.profile.save()
        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['data'] = subdoc            
        json_document['badges'] = self.badge_list
        json_document['experience_points'] = xp_points
        return HttpResponse(
            content=render_as_json(json_document),
            content_type='application/json',
        )

    def validate_fields(self,data):
        if data['modification_type'] == "Modification":
            for entry in data.keys():
                if (entry in strings.MODIFICATION_MANDATORY_FIELDS and 
                    data[entry] == ""):
                    return False, entry
                elif ((entry == 'place' or entry == 'user') and 
                    data[entry]['id'] == ""):
                    return False, entry
            return True, None
        elif data['modification_type'] == "UnexistantPlace":
            for entry in data.keys():
                if (entry in strings.UNEXISTANT_PLACE_MANDATORY_FIELDS and 
                    data[entry] == ""):
                    return False, entry
                elif ((entry == 'place' or entry == 'user') and 
                    data[entry]['id'] == ""):
                    return False, entry
            return True, None
        else:
            return False,"modification_type"

class PhotoModificationPlaceController(
    AuthMixIn,
    ControllerResourceView):
    
    def run(self, request, *args, **kwargs):
        modification_id = kwargs.get('mod_id', 0)

        photo = request.FILES['photo']
        try:
            modification = models.ModificationPlace.objects.get(id=modification_id)
            if not modification.modification_type == 1:
                error = OrderedDict()
                message = unicode('Modification %s is an unexistant place not a modification' % modification_id)
                error['message'] = message
                error_list = [error]
                return HttpResponseConflict(
                    content=json_error_document(error_list),
                    content_type='application/json'
                ) 
            modification.photo = photo
            modification.save()
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                ModificationPlaceResource.short_json_representation(modification,False)
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )
        except models.ModificationPlace.DoesNotExist:
            error = OrderedDict()
            message = strings.NON_EXISTANT_MODIFICATION % (modification_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json'
            )

#VALIDATION MODIFICATION PLACE

class ValidationModificationPlaceController(
    AuthMixIn,
    ControllerResourceView):

    badge_list = []

    def award_badge(self,sender,**kwargs):
        from users.badge_resources.resources import BadgeResource
        self.badge_list.append(BadgeResource.short_json_representation(
            kwargs['badge'],False)
        )
    def run(self, request, *args, **kwargs):
        from badges.signals import badge_awarded
        from users.meta_badges import Veteran
        badge_awarded.connect(self.award_badge, sender=Veteran)

        data = json.loads(request.raw_post_data)
        val_keys, entry = self.validate_fields(data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        vote=data['vote'].lower()
        try:
            modification = models.ModificationPlace.objects.get(
                id=data['modification']['id']
            )
        except models.ModificationPlace.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_MODIFICATION % data['modification']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            user = User.objects.get(
                id=data['user']['id']
            )
        except User.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_USER % data['user']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            validmodification = models.ValidationModificationPlace.objects.get(
                user=user.id,
                modification_place=modification.id
            )
            xp_points = 0
            validmodification.vote = \
                models.ValidationModificationPlace.VOTE_CHOICES.get_value(vote)
            validmodification.date = datetime.now()
            validmodification.save()
        except models.ValidationModificationPlace.DoesNotExist:
            validmodification = models.ValidationModificationPlace.create_new_validation(
                modification=modification,
                user=user,
                vote=vote
            )
            xp_points = settings.EXPERIENCE_POINTS_EARNED['validate_modification']
            user.profile.experience_points+=\
                xp_points
            user.profile.experience_points_week+=\
                xp_points
            user.profile.save()

        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['badges'] = self.badge_list
        json_document['experience_points'] = xp_points
        return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.MOD_VALIDATION_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif (entry == 'user' or entry == 'modification') and data[entry]['id']=="":
                return False, entry
        return True, None

class PlaceTypeCollection(
    AuthMixIn,
    ControllerResourceView):
    def get(self, request, *args, **kwargs):
        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['data'] = \
            PlaceTypeCollection.json_representation()
        json_document['link'] = \
            self.get_restful_link_metadata(rel='self')
        return HttpResponse(
            content=render_as_json(json_document),
            content_type='application/json'
        )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('placetypes')
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.PLACE_TYPE_COLLECTION)
        metadata['type'] = 'application/json'
        return metadata
        
    @staticmethod
    def json_representation(render_json=True):

        placetypes = models.PlaceType.objects.all().order_by('description')
        placetypes_list = []
        for placetype in placetypes:
            placetypes_list.append({
                "description":placetype.description
            })
        return placetypes_list
