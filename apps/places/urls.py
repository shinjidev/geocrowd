from django.conf.urls import patterns, include, url

from places import resources

urlpatterns = patterns('',

    #PLaces Modifications
    url(r'^modifications/$',
        resources.ModificationPlaceCollection.as_view(),
        name='modifications'),
    url(r'^modifications/add/$',
        resources.ModificationPlaceController.as_view(),
        name='add_modifications'),
    url(r'^modifications/(?P<mod_id>[-\w]+)/addphoto/$',
        resources.PhotoModificationPlaceController.as_view(),
        name='add_modifications_photo'),
    url(r'^modifications/(?P<mod_id>[-\w]+)/$',
        resources.ModificationPlaceResource.as_view(),
        name='modifications'),
    url(r'^modifications/(?P<mod_id>[-\w]+)/vote/$',
        resources.ValidationModificationPlaceController.as_view(),
        name='vote_modifications'),
    url(r'^(?P<place_id>[-\w]+)/modifications/$',
        resources.ModificationPlaceCollection.as_view(),
        name='modifications'),

    #Place Types
    url(r'^placetypes/$',
        resources.PlaceTypeCollection.as_view(),
        name='placetypes'),

    #PLaces
    url(r'^$',
        resources.PlaceCollection.as_view(),
        name='places'),
    url(r'^nearest/$',
        resources.NearPlacesCollection.as_view(),
        name='nearest'),
    url(r'^add/$',
        resources.PlaceController.as_view(),
        name='add'),
    url(r'^(?P<place_id>[-\w]+)/addphoto/$',
        resources.PhotoPlaceController.as_view(),
        name='addphoto'),
    url(r'^(?P<place_id>[-\w]+)/vote/$',
        resources.ValidationPlaceController.as_view(),
        name='vote'),
    url(r'^(?P<place_id>[-\w]+)/like/$',
        resources.VotePlaceController.as_view(),
        name='like'),
    url(r'^(?P<place_id>[-\w]+)/$',
        resources.PlaceResource.as_view(),
        name='places'),
)
