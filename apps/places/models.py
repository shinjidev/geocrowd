from ordereddict import OrderedDict
import mimetypes

import django.utils.simplejson as json
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django_countries import CountryField
from sorl.thumbnail.fields import ImageField
from django.db.models.fields import BooleanField
from django.conf import settings
from django.core.urlresolvers import reverse

from common.utils.photos import uuid_based_picture_name

from common.datastructures.enumeration import Enumeration
from places import strings


class PlaceType(models.Model):
    """
    Class that stores the type of place, used later in every
    place instance.
    """

    description=models.CharField(
        verbose_name=strings.PLACE_DESCRIPTION,
        max_length=256,
    )

    photo = ImageField(
        verbose_name=strings.PLACE_PHOTO,
        upload_to=uuid_based_picture_name('photos/placetypes'),
        blank=True,
        null=True,
    )

    def __unicode__(self):
        return u'%s' % (self.description,)

    def get_photo_restful_url(self):
        return '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            self.photo.url
        )

    def get_photo_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_photo_restful_url()
        metadata['rel'] = rel
        metadata['title'] = self.photo.name.split('/')[-1]
        metadata['type'] = mimetypes.guess_type(self.photo.url)[0]
        return metadata

    def get_json_representation(self, render_json=True):
        document = OrderedDict()
        document['id'] = self.id
        document['description'] = self.description
        # Icon
        if self.photo:
            subdoc = OrderedDict()
            subdoc['name'] = self.photo.url.split('/')[-1]
            subdoc['link'] = self.get_photo_restful_link_metadata(rel='self')
            document['photo'] = subdoc
        else:
            document['photo'] = ""
        
        if render_json:
            return render_as_json(document)
        else:
            return document

    def get_photo(self):
        if self.photo:
            return u'<img src="%s" height="25"/>' \
                % self.get_photo_restful_url()
        else:
            return '(No photo)'
    get_photo.allow_tags = True

class Place(models.Model):

    name=models.CharField(
        verbose_name=strings.PLACE_NAME,
        max_length=256,
    )

    STATE_CHOICES = Enumeration([
        (1, 'VALIDATED', strings.STATE_VALIDATED),
        (2, 'TO_BE_VALIDATED', strings.STATE_TO_BE_VALIDATED),
        (3, 'UNVALIDATED_MODIFICATION', 
            strings.STATE_UNVALIDATED_MODIFICATION)
    ])

    state=models.PositiveIntegerField(
        verbose_name=strings.PLACE_STATE,
        choices=STATE_CHOICES,
        default=2,
    )

    coordinates = models.PointField(
        help_text="Represented as POINT (longitude latitude)",
        verbose_name=strings.PLACE_COORDINATES,
    )

    user=models.ForeignKey(User,
        verbose_name=strings.PLACE_USER,
    )

    description=models.TextField(
        verbose_name=strings.PLACE_DESCRIPTION,
        blank=True,
        null=True,
    )

    place_type=models.ForeignKey(PlaceType,
        verbose_name=strings.PLACE_TYPE,
    )

    address=models.TextField(
        verbose_name=strings.PLACE_ADDRESS,
        blank=True,
        null=True,
    )

    country = CountryField(
        verbose_name=strings.PLACE_COUNTRY,
        blank=True,
        null=True,
    )

    likes = models.PositiveIntegerField(
        verbose_name=strings.PLACE_LIKE,
        editable=False,
        default=0,
    )

    dislikes = models.PositiveIntegerField(
        verbose_name=strings.PLACE_DISLIKE,
        editable=False,
        default=0,
    )

    photo = ImageField(
        verbose_name=strings.PLACE_PHOTO,
        upload_to=uuid_based_picture_name('photos/places'),
        blank=True,
        null=True,
    )

    visibility=BooleanField(
        verbose_name=strings.PLACE_VISIBILITY,
        default=False,
    )

    objects = models.GeoManager()
    
    class Meta:
        verbose_name = strings.PLACE_VERBOSE_NAME
        verbose_name_plural = strings.PLACE_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.name,)

    def get_photo_restful_url(self):
        return '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            self.photo.url
        )

    def get_photo_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_photo_restful_url()
        metadata['rel'] = rel
        metadata['title'] = self.photo.file.name.split('/')[-1]
        metadata['type'] = mimetypes.guess_type(self.photo.file.name)[0]
        return metadata

    def get_restful_url(self):
        return '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('places', kwargs={'place_id':self.id})
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = self.name
        metadata['type'] = 'application/json'
        return metadata

    def get_coordinates(self):
        coordinates = OrderedDict()
        coordinates['longitude'] = self.coordinates.x
        coordinates['latitude'] = self.coordinates.y
        return coordinates

    def get_likes_difference(self):
        return (self.likes - self.dislikes)

    @staticmethod
    def create_new_place(
        name,
        user,
        coordinates,
        place_type,
        description=None,
        address=None,
        country=None,
        visibility=False,
        state=2,
        photo=None
        ):

        new_place = Place(
            name=name,
            user=user,
            coordinates=coordinates,
            place_type=place_type,
            description=description,
            address=address,
            country=country,
            visibility=visibility,
            state=state,
            photo=photo
        )
        new_place.save()
        return new_place

    def get_photo(self):
        if self.photo:
            return u'<img src="%s" height="75"/>' \
                % self.get_photo_restful_url()
        else:
            return '(No photo)'
    get_photo.allow_tags = True

class VotePlace(models.Model):
    user=models.ForeignKey(User,
        verbose_name=strings.PLACE_USER,
    )

    place=models.ForeignKey(Place,
        verbose_name=strings.PLACE_NAME,
    )

    date = models.DateTimeField(
        verbose_name=strings.PLACE_DATE,
        auto_now_add=True,
        editable=False,
    )

    VOTE_CHOICES = Enumeration([
        (1, 'like', strings.PLACE_LIKE),
        (2, 'dislike', strings.PLACE_DISLIKE),
    ])

    vote=models.PositiveIntegerField(
        verbose_name=strings.PLACE_VOTE,
        choices=VOTE_CHOICES,
        default=1,
    )

    class Meta:
        verbose_name = strings.PLACE_VOTE_VERBOSE_NAME
        verbose_name_plural = \
            strings.PLACE_VOTE_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.VOTE_CHOICES.get_key(self.vote),)

    @staticmethod
    def create_new_vote(
        place,
        user,
        vote
        ):
        vote_value=VotePlace.VOTE_CHOICES.get_value(vote)
        new_vote = VotePlace(
            place=place,
            user=user,
            vote=vote_value
        )
        new_vote.save()
        return new_vote

class ValidationPlace(models.Model):
    """
    Class that stores a place validation request place
    made by a user.
    """

    user=models.ForeignKey(User,
        verbose_name=strings.PLACE_USER,
    )

    place=models.ForeignKey(Place,
        verbose_name=strings.PLACE_NAME,
    )

    date = models.DateTimeField(
        verbose_name=strings.PLACE_DATE,
        auto_now_add=True,
        editable=False,
    )

    VOTE_CHOICES = Enumeration([
        (1, 'yeah', strings.PLACE_YEAH),
        (2, 'nay', strings.PLACE_NAY),
    ])

    vote=models.PositiveIntegerField(
        verbose_name=strings.PLACE_VOTE,
        choices=VOTE_CHOICES,
        default=1,
    )

    class Meta:
        verbose_name = strings.PLACE_VALIDATION_VERBOSE_NAME
        verbose_name_plural = \
            strings.PLACE_VALIDATION_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.VOTE_CHOICES.get_key(self.vote),)

    @staticmethod
    def create_new_validation(
        place,
        user,
        vote
        ):
        vote_value=ValidationPlace.VOTE_CHOICES.get_value(vote)
        new_validation = ValidationPlace(
            place=place,
            user=user,
            vote=vote_value
        )
        new_validation.save()
        return new_validation

class ModificationPlace(models.Model):
    """
    Class that stores a place modification request
    made by a user.
    """

    TYPE_CHOICES = Enumeration([
        (1, 'Modification', strings.PLACE_MODIFICATION_VERBOSE_NAME),
        (2, 'UnexistantPlace', strings.UNEXISTANT_PLACE_VERBOSE_NAME),
    ])

    modification_type = models.PositiveIntegerField(
        verbose_name=strings.MODIFICATION_TYPE,
        choices=TYPE_CHOICES,
        default=1,
    )

    STATE_CHOICES = Enumeration([
        (1, 'VALIDATED', strings.STATE_VALIDATED),
        (2, 'TO_BE_VALIDATED', strings.STATE_TO_BE_VALIDATED),
        (3, 'UNVALIDATED_MODIFICATION', 
            strings.STATE_UNVALIDATED_MODIFICATION)
    ])

    state=models.PositiveIntegerField(
        verbose_name=strings.PLACE_STATE,
        choices=STATE_CHOICES,
        default=2,
    )

    place = models.ForeignKey(Place,
        verbose_name=strings.PLACE_VERBOSE_NAME,
    )

    name=models.CharField(
        verbose_name=strings.PLACE_NAME,
        max_length=256,
        blank=True,
        null=True,
    )

    coordinates = models.PointField(
        help_text="Enter as: \'POINT (longitude latitude)\'",
        verbose_name=strings.PLACE_COORDINATES,
        blank=True,
        null=True,
    )

    user=models.ForeignKey(User,
        verbose_name=strings.PLACE_USER,
    )

    description=models.TextField(
        verbose_name=strings.PLACE_DESCRIPTION,
        blank=True,
        null=True,
    )

    place_type=models.ForeignKey(PlaceType,
        verbose_name=strings.PLACE_TYPE,
        blank=True,
        null=True,
    )

    address=models.TextField(
        verbose_name=strings.PLACE_ADDRESS,
        blank=True,
        null=True,
    )

    country = CountryField(
        verbose_name=strings.PLACE_COUNTRY,
        blank=True,
        null=True,
    )

    photo = ImageField(
        verbose_name=strings.PLACE_PHOTO,
        upload_to=uuid_based_picture_name('photos/places'),
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = strings.PLACE_MODIFICATION_VERBOSE_NAME
        verbose_name_plural = \
            strings.PLACE_MODIFICATION_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s %s' % (self.TYPE_CHOICES.get_key(self.modification_type),self.name,)

    def mod_type(self):
        return self.TYPE_CHOICES.get_key(self.modification_type)

    def get_photo_restful_url(self):
        return '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            self.photo.url
        )

    def get_photo_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_photo_restful_url()
        metadata['rel'] = rel
        metadata['title'] = self.photo.file.name.split('/')[-1]
        metadata['type'] = mimetypes.guess_type(self.photo.file.name)[0]
        return metadata

    def get_restful_url(self):
        return '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('modifications', kwargs={'mod_id':self.id})
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = self.name
        metadata['type'] = 'application/json'
        return metadata

    def get_coordinates(self):
        if self.coordinates:
            coordinates = OrderedDict()
            coordinates['longitude'] = self.coordinates.x
            coordinates['latitude'] = self.coordinates.y
            return coordinates
        return ""

    @staticmethod
    def create_new_modification(
        modification_type,
        place,
        user,
        state=2,
        name=None,
        coordinates=None,
        place_type=None,
        description=None,
        address=None,
        country=None,
        photo=None
        ):

        new_modification = ModificationPlace(
            modification_type=modification_type,
            state=state,
            place=place,
            user=user,
            name=name,
            coordinates=coordinates,
            place_type=place_type,
            description=description,
            address=address,
            country=country,
            photo=photo
        )
        new_modification.save()
        return new_modification

    def validate(self):
        self.state=1
        place = self.place
        place.user = self.user
        place.name = self.name
        place.coordinates = self.coordinates
        place.place_type = self.place_type
        place.description = self.description
        place.address = self.address
        place.country = self.country
        place.photo = self.photo
        place.save()
        self.save()
    

    def get_photo(self):
        if self.photo:
            return u'<img src="%s" height="75"/>' \
                % self.get_photo_restful_url()
        else:
            return '(No photo)'
    get_photo.allow_tags = True


class ValidationModificationPlace(models.Model):
    """
    Class that stores a place modification validation request
    made by a user.
    """

    modification_place = models.ForeignKey(ModificationPlace,
        verbose_name=strings.PLACE_VERBOSE_NAME,
    )

    user=models.ForeignKey(User,
        verbose_name=strings.PLACE_USER,
    )

    date = models.DateTimeField(
        verbose_name=strings.PLACE_DATE,
        auto_now_add=True,
        editable=False,
    )

    VOTE_CHOICES = Enumeration([
        (1, 'yeah', strings.PLACE_YEAH),
        (2, 'nay', strings.PLACE_NAY),
    ])

    vote=models.PositiveIntegerField(
        verbose_name=strings.PLACE_VOTE,
        choices=VOTE_CHOICES,
        default=1,
    )

    class Meta:
        verbose_name = strings.PLACE_MOD_VALIDATION_VERBOSE_NAME
        verbose_name_plural = \
            strings.PLACE_MOD_VALIDATION_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.VOTE_CHOICES.get_key(self.vote),)

    @staticmethod
    def create_new_validation(
        modification,
        user,
        vote
        ):
        vote_value=ValidationModificationPlace.VOTE_CHOICES.get_value(vote)
        new_validation = ValidationModificationPlace(
            modification_place=modification,
            user=user,
            vote=vote_value
        )
        new_validation.save()
        return new_validation

"""class UnexistantPlace(models.Model):
    
    Class that stores a place modification that states that a
    place doesn't exist.
    

    place = models.ForeignKey(Place,
        verbose_name=strings.PLACE_VERBOSE_NAME,
    )

    user=models.ForeignKey(User,
        verbose_name=strings.PLACE_USER,
    )

    #Reason why the place shouldn't exist.
    reason=models.TextField(
        verbose_name=strings.UNEXISTANT_PLACE_REASON,
    )

    date = models.DateTimeField(
        verbose_name=strings.PLACE_DATE,
        auto_now_add=True,
        editable=False,
    )

    class Meta:
        verbose_name = strings.UNEXISTANT_PLACE_VERBOSE_NAME
        verbose_name_plural = \
            strings.UNEXISTANT_PLACE_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.reason,)

    @staticmethod
    def create_new_unexistant(
        place,
        user,
        reason
        ):
        new_unexistant = UnexistantPlace(
            place=place,
            user=user,
            reason=reason
        )
        new_unexistant.save()
        return new_unexistant"""