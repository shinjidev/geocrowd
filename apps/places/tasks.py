from celery.task import task

from django.conf import settings

from places.models import Place, ValidationPlace
from places.models import ModificationPlace, ValidationModificationPlace

@task
def validate_places():
	places = Place.objects.filter(state=2)
	for place in places:
		yeahs=ValidationPlace.objects.filter(place=place,vote=1)
		nays=ValidationPlace.objects.filter(place=place,vote=2)
		yeahs_average=0
		num_yeahs=0
		nays_average=0
		num_nays=0
		for yeah in yeahs:
			yeahs_average += yeah.user.profile.get_kharma_average()
			num_yeahs+=1
		for nay in nays:
			nays_average += nay.user.profile.get_kharma_average()
			num_nays+=1
		if not num_yeahs == 0: yeahs_average/=num_yeahs
		if not num_nays == 0: nays_average/=num_nays
		diff_votes=yeahs_average-nays_average
		if diff_votes >= settings.VALIDATION_POINTS_REQUIRED['places'] or place.user.profile.kharma_points>=100:
			place.state = 1
			place.visibility = True
			place.save()
			profile = place.user.profile
			profile.num_validated_places+=1
			profile.experience_points+=\
				settings.EXPERIENCE_POINTS_EARNED['own_place_validated']
			profile.experience_points_week+=\
				settings.EXPERIENCE_POINTS_EARNED['own_place_validated']
			profile.kharma_points+=\
				settings.KHARMA_POINTS_EARNED['own_place_validated']
			profile.save()
			for yeah in yeahs:
				profile2 = yeah.user.profile
				profile2.kharma_points+=\
					settings.KHARMA_POINTS_EARNED['voted_place_validated']
				profile2.save()
			for nay in nays:
				profile2 = nay.user.profile
				profile2.kharma_points-=\
					settings.KHARMA_POINTS_LOST['voted_place_unvalidated']
				profile2.save()
		if diff_votes <= (-1*settings.VALIDATION_POINTS_REQUIRED['places']):
			place.state = 3
			place.visibility = False
			place.save()
			profile = place.user.profile
			profile.kharma_points-=\
				settings.KHARMA_POINTS_EARNED['own_place_validated']
			profile.save()
			for yeah in yeahs:
				profile2 = yeah.user.profile
				profile2.kharma_points-=\
					settings.KHARMA_POINTS_LOST['voted_place_unvalidated']
				profile2.save()
			for nay in nays:
				profile2 = nay.user.profile
				profile2.kharma_points+=\
					settings.KHARMA_POINTS_EARNED['voted_place_validated']
				profile2.save()

@task
def validate_modifications():
	modifications = ModificationPlace.objects.filter(state=2)
	for modification in modifications:
		yeahs=ValidationModificationPlace.objects.filter(modification_place=modification,vote=1)
		nays=ValidationModificationPlace.objects.filter(modification_place=modification,vote=2)
		yeahs_average=0
		num_yeahs=0
		nays_average=0
		num_nays=0
		for yeah in yeahs:
			yeahs_average += yeah.user.profile.get_kharma_average()
			num_yeahs+=1
		for nay in nays:
			nays_average += nay.user.profile.get_kharma_average()
			num_nays+=1
		if not num_yeahs == 0: yeahs_average/=num_yeahs
		if not num_nays == 0: nays_average/=num_nays
		diff_votes=yeahs_average-nays_average
		if diff_votes >= settings.VALIDATION_POINTS_REQUIRED['modifications']:
			modification.validate()
			profile = modification.user.profile
			profile.experience_points+=\
				settings.EXPERIENCE_POINTS_EARNED['own_modification_validated']
			profile.experience_points_week+=\
				settings.EXPERIENCE_POINTS_EARNED['own_modification_validated']
			profile.kharma_points+=\
				settings.KHARMA_POINTS_EARNED['own_modification_validated']
			profile.save()
			for yeah in yeahs:
				profile2 = yeah.user.profile
				profile2.kharma_points+=\
					settings.KHARMA_POINTS_EARNED['voted_modification_validated']
				profile2.save()
			for nay in nays:
				profile2 = nay.user.profile
				profile2.kharma_points-=\
					settings.KHARMA_POINTS_EARNED['voted_modification_unvalidated']
				profile2.save()
		if diff_votes <= (-1*settings.VALIDATION_POINTS_REQUIRED['modifications']):
			modification.state = 3
			modification.save()
			profile = modification.user.profile
			profile.kharma_points-=\
				settings.KHARMA_POINTS_EARNED['own_modification_validated']
			profile.save()
			for yeah in yeahs:
				profile2 = yeah.user.profile
				profile2.kharma_points-=\
					settings.KHARMA_POINTS_LOST['voted_modification_unvalidated']
				profile2.save()
			for nay in nays:
				profile2 = nay.user.profile
				profile2.kharma_points+=\
					settings.KHARMA_POINTS_EARNED['voted_modification_unvalidated']
				profile2.save()