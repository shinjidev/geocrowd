from django.contrib import admin
#from django.contrib.gis import admin as geoadmin
from places.models import *

class PlaceTypeAdmin(admin.ModelAdmin):
	list_display=('description','get_photo',)

admin.site.register(PlaceType,PlaceTypeAdmin)

class PlaceAdmin(admin.ModelAdmin):
	list_display=('name','user','get_photo',)
	readonly_fields=('likes','dislikes',)

admin.site.register(Place,PlaceAdmin)

class VotePlaceAdmin(admin.ModelAdmin):
	list_display=('__unicode__','user','place',)
	readonly_fields=('date',)

admin.site.register(VotePlace,VotePlaceAdmin)

class ValidationPlaceAdmin(admin.ModelAdmin):
	list_display=('__unicode__','user','place',)
	readonly_fields=('date',)

admin.site.register(ValidationPlace,ValidationPlaceAdmin)

class ModificationPlaceAdmin(admin.ModelAdmin):
	search_fields = ('name',)
	list_display=('name','user','mod_type','get_photo',)

admin.site.register(ModificationPlace,ModificationPlaceAdmin)

class ValidationModificationPlaceAdmin(admin.ModelAdmin):
	readonly_fields=('date',)

admin.site.register(ValidationModificationPlace,ValidationModificationPlaceAdmin)
