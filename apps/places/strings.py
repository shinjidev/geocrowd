from django.utils.translation import ugettext_lazy as _

PLACE_MANDATORY_FIELDS = ['name','position','type','user']
PLACE_NULL_FIELDS = ['description','address','country','photo']

VOTE_MANDATORY_FIELDS = ['like','user','place']
VALIDATION_MANDATORY_FIELDS = ['vote','user','place']

MODIFICATION_MANDATORY_FIELDS = ['place','user']
UNEXISTANT_PLACE_MANDATORY_FIELDS = ['place','user','description']
MOD_VALIDATION_MANDATORY_FIELDS = ['vote','user','modification']

PLACE_NAME = _(u'Name')
PLACE_STATE = _(u'State')
PLACE_ADDRESS = _(u'Address')
PLACE_COUNTRY = _(u'Country')
PLACE_COORDINATES = _(u'Coordinates')
PLACE_DESCRIPTION = _(u'Description')
PLACE_TYPE = _(u'Type')
PLACE_LIKE = _(u'Likes')
PLACE_DISLIKE = _(u'Dislikes')
PLACE_PHOTO = _(u'Photo')
PLACE_VISIBILITY = _(u'Visibility')

#Votes
PLACE_VOTE = _(u'Vote')
PLACE_USER = _(u'User')
PLACE_DATE = _(u'Date')

#Validation
PLACE_YEAH= _(u'Yeah (Positive votes)')
PLACE_NAY= _(u'Nay (Negative votes)')

PLACE_VERBOSE_NAME = _(u'Place')
PLACE_VERBOSE_NAME_PLURAL = _(u'Places')
PLACE_VOTE_VERBOSE_NAME = _(u'Place vote')
PLACE_VOTE_VERBOSE_NAME_PLURAL = _(u'Place votes')
PLACE_VALIDATION_VERBOSE_NAME = _(u'Place validation')
PLACE_VALIDATION_VERBOSE_NAME_PLURAL = _(u'Place validations')
PLACE_MODIFICATION_VERBOSE_NAME = _(u'Place modification')
PLACE_MODIFICATION_VERBOSE_NAME_PLURAL = _(u'Place modifications')
PLACE_MOD_VALIDATION_VERBOSE_NAME = _(u'Place modification validation')
PLACE_MOD_VALIDATION_VERBOSE_NAME_PLURAL = _(u'Place modification validations')

STATE_VALIDATED = _(u'Validated')
STATE_TO_BE_VALIDATED = _(u'To be validated')
STATE_UNVALIDATED_MODIFICATION = _(u'Unvalidated modification')

NON_EXISTANT_PLACE = _(u'Place %s does not exist!')
PLACE_EXISTS = _(u'Place already exists!')
PLACE_TYPE_DOESNT_EXISTS = _(u'The place type does not exist!')
PLACE_COLLECTION = _(u'A collection of all places.')

NON_EXISTANT_USER = _(u'User %s does not exist!')

MODIFICATION_EXISTS = _(u'Modification already exists!')
MODIFICATION_TYPE = _(u'Modification Type')
MODIFICATIONS_COLLECTION = _(u'A collection of all places modifications.')
NON_EXISTANT_MODIFICATION = _(u'Modification %s does not exist!')

NEAR_PLACES_COLLECTION = _(u'A collection of all places near to a given location.')
PLACE_TYPE_COLLECTION = _(u'A collection of all place types (categories).')

UNEXISTANT_PLACE_VERBOSE_NAME = _(u'Unexistant Place')