import django.utils.simplejson as json

from datetime import datetime
from ordereddict import OrderedDict
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from django.http import HttpResponseBadRequest
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.auth.models import User

from rest.views import ResourceView
from rest.views import ControllerResourceView
from rest.views import CollectionResourceView

from rest.http import HttpResponseConflict
from rest.http import HttpResponseNoContent
from rest.auth import RESTfulBasicOrDjangoAuthenticationMixIn as AuthMixIn
from rest.utils import render_as_json
from rest.utils import json_error_document

from users.resources import UserResource
from places.resources import PlaceResource
from places.models import Place
from comment.models import Comment, VoteComment
from comment import strings

class CommentResource(
    AuthMixIn, 
    ResourceView):

    def get(self, request, *args, **kwargs):
        comment_id = kwargs.get('comment_id', 0)
        try:
            comment = Comment.objects.get(id=comment_id)
            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                CommentResource.json_representation(comment,False)
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )
        except Comment.DoesNotExist:
            error = OrderedDict()
            message = strings.NON_EXISTANT_COMMENT % (comment_id)
            error['message'] = message
            error_list = [error]
            return HttpResponseNotFound(
                content=json_error_document(error_list),
                content_type='application/json'
            )

    @staticmethod
    def json_representation(comment, render_json=True):
        # Document
        document = OrderedDict()
        document['id'] = comment.id
        document['description'] = comment.description
        document['date'] = comment.date 
        document['visibility'] = comment.visibility
        document['likes'] = comment.likes
        document['dislikes'] = comment.dislikes
        document['user'] = \
            UserResource.short_json_representation(comment.user.profile, False)
        document['place'] = \
            PlaceResource.short_json_representation(comment.place, False)
        document['link'] = comment.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document


    @staticmethod
    def short_json_representation(comment, render_json=True):
        document = OrderedDict()
        document['id'] = comment.id
        document['description'] = comment.description
        document['likes'] = comment.likes
        document['dislikes'] = comment.dislikes
        document['user'] = \
            UserResource.photo_short_json_representation(comment.user.profile, False)
        document['link'] = comment.get_restful_link_metadata(rel='self')
        if render_json:
            return render_as_json(document)
        else:
            return document

class CommentCollection(CollectionResourceView):

    def get(self, request, *args, **kwargs):
        
        json_document = OrderedDict()
        json_document['status'] = 'success'
        json_document['data'] = OrderedDict()
        json_document['data']['comments'] = \
            CommentCollection.json_representation()
        json_document['link'] = \
            self.get_restful_link_metadata(rel='self')
        print json_document['link']
        return HttpResponse(
            content=render_as_json(json_document),
            content_type='application/json'
        )

    def get_restful_url(self):
        return "%s%s" % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('comments')
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = unicode(strings.COMMENT_COLLECTION)
        metadata['type'] = 'application/json'
        print metadata
        return metadata
        
    @staticmethod
    def json_representation(place_id=None,user_id=None,render_json=True):

        if place_id and user_id:
            comments = Comment.objects.filter(place=place_id,user=user_id)
        elif place_id:
            comments = Comment.objects.filter(place=place_id)
        elif user_id:
            comments = Comment.objects.filter(user=user_id)
        else:
            comments = Comment.objects.all()
        comments_list = []
        for comment in comments:
            comments_list.append({"comment":
                CommentResource.short_json_representation(
                    comment,
                    False
                )}
            )
        return comments_list

class CommentController(
    AuthMixIn,
    ControllerResourceView):
    
    badge_list = []

    def award_badge(self,sender,**kwargs):
        from users.badge_resources.resources import BadgeResource
        self.badge_list.append(BadgeResource.short_json_representation(
            kwargs['badge'],False)
        )

    def run(self, request, *args, **kwargs):
        from badges.signals import badge_awarded
        from users.meta_badges import Commentator, Veteran
        badge_awarded.connect(self.award_badge, sender=Commentator)
        badge_awarded.connect(self.award_badge, sender=Veteran)

        data = json.loads(request.raw_post_data.decode("latin-1"))
        cleaned_data = data['comment']
        val_keys, entry = self.validate_fields(cleaned_data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        username = cleaned_data['user']['username']
        place_id =cleaned_data['place']['id']
        comment_description = cleaned_data['description']
        missing, user, place = self.valid_user_place(username,place_id) 
        if len(missing) > 0:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.INVOLVED_DONT_EXIST % ",".join(missing))
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json'
            )

        if self.valid_comment(comment_description,place,user):
            comment = Comment.create_new_comment(
                description=comment_description,
                user=user,
                place=place
            )
            xp_points = settings.EXPERIENCE_POINTS_EARNED['add_comment']
            user.profile.experience_points+=\
                xp_points
            user.profile.experience_points_week+=\
                xp_points
            user.profile.save()

            json_document = OrderedDict()
            json_document['status'] = 'success'
            json_document['data'] = \
                CommentResource.short_json_representation(comment,False)
            json_document['badges'] = self.badge_list
            json_document['experience_points'] = xp_points
            return HttpResponse(
                content=render_as_json(json_document),
                content_type='application/json',
            )
        else:
            error = OrderedDict()
            message = (strings.COMMENT_EXISTS % place.name)
            error['message'] = unicode(message)
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json'
            )

    def valid_user_place(self,username,place_id):
        missing = []
        val_flag = True
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            missing.append('user')
            user=None
            val_flag = False
        try:
             place =Place.objects.get(id=place_id)
        except Place.DoesNotExist:
            missing.append('place')
            place=None
            val_flag = False
        return missing, user, place


    def valid_comment(self,description,place,user):
        if Comment.objects.filter(description=description,place=place.id,user=user.id).exists():
            return False
        return True

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.COMMENT_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif (entry == 'user' and data[entry]['username'] == ""):
                return False, entry
            elif (entry == 'place' and data[entry]['id'] == ""):
                return False, entry
        return True, None

class VoteCommentController(
    AuthMixIn,
    ControllerResourceView):

    def run(self, request, *args, **kwargs):
        data = json.loads(request.raw_post_data)
        val_keys, entry = self.validate_fields(data)
        if not val_keys:
            error = OrderedDict()
            message = unicode(" ".join(["The",entry,"field is mandatory!"]))
            error['message'] = message
            error_list = [error]
            return HttpResponseBadRequest(
                content=json_error_document(error_list),
                content_type='application/json',
            )

        vote=data['like'].lower()
        try:
            comment = Comment.objects.get(
                id=data['comment']['id']
            )
        except Comment.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_COMMENT % data['comment']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        try:
            user = User.objects.get(
                id=data['user']['id']
            )
        except User.DoesNotExist:
            error = OrderedDict()
            error['message'] = \
                unicode(strings.NON_EXISTANT_USER % data['user']['id'])
            error_list = [error]
            return HttpResponseConflict(
                content=json_error_document(error_list),
                content_type='application/json',
            )
        likes = {"like":0,"dislike":0}
        vote_value = VoteComment.VOTE_CHOICES.get_value(vote)
        try:
            votecomment = VoteComment.objects.get(
                user=user.id,
                comment=comment.id
            )
            if votecomment.vote != vote_value:
                votecomment.vote = vote_value
                votecomment.date = datetime.now()
                likes['like'] = likes['dislike'] = -1
                likes[vote]=1
                votecomment.save()
        except VoteComment.DoesNotExist:
            votecomment = VoteComment.create_new_vote(
                comment=comment,
                user=user,
                vote=vote,
            )
            likes[vote] = 1

        comment.likes+=likes['like']
        comment.dislikes+=likes['dislike']
        comment.save()
        if not comment.votes_reached or not comment.dislikes_reached:
            self.check_kharma_points(comment)
        return HttpResponseNoContent()

    def validate_fields(self,data):
        for entry in data.keys():
            if entry in strings.VOTE_MANDATORY_FIELDS and data[entry] == "":
                return False, entry
            elif (entry == 'user' or entry == 'comment') and data[entry]['id']=="":
                return False, entry
        return True, None

    def check_kharma_points(self,comment):
        if not comment.votes_reached:
            votes = VoteComment.objects.filter(comment=comment)
            lk_kharma = 0
            num_lk = 0.0
            dlk_kharma = 0
            num_dlk = 0.0
            for vote in votes:
                if vote.vote == 1:
                    lk_kharma+=vote.user.profile.get_kharma_average()
                    num_lk+=1
                if vote.vote == 2:
                    dlk_kharma+=vote.user.profile.get_kharma_average()
                    num_dlk+=1
            if not num_lk==0:
                lk_kharma/=num_lk  
            if not num_dlk==0:
                dlk_kharma/=num_dlk
            if (lk_kharma-dlk_kharma) > settings.VALIDATION_POINTS_REQUIRED['comments']:
                comment.votes_reached = True
                comment.dislikes_reached = False
                comment.save()
                profile = comment.user.profile
                profile.kharma_points += settings.KHARMA_POINTS_EARNED['own_comment_voted']
                profile.save()
                for vote in votes:
                    profile2 = vote.user.profile
                    if vote.vote == 1:
                        profile2.kharma_points+=settings.KHARMA_POINTS_EARNED['voted_comment_validated']
                    if vote.vote == 2:
                        profile2.kharma_points-=settings.KHARMA_POINTS_LOST['voted_comment_unvalidated']
                    profile2.save()

        if not comment.dislikes_reached:
            votes = VoteComment.objects.filter(comment=comment)
            lk_kharma = 0
            num_lk = 0.0
            dlk_kharma = 0
            num_dlk = 0.0
            for vote in votes:
                if vote.vote == 1:
                    lk_kharma+=vote.user.profile.get_kharma_average()
                    num_lk+=1
                if vote.vote == 2:
                    dlk_kharma+=vote.user.profile.get_kharma_average()
                    num_dlk+=1
            if not num_lk==0:
                lk_kharma/=num_lk
            if not num_dlk==0:                
                dlk_kharma/=num_dlk
            if (dlk_kharma-lk_kharma) > settings.VALIDATION_POINTS_REQUIRED['comments']:
                comment.votes_reached = False
                comment.dislikes_reached = True
                comment.save()
                profile = comment.user.profile
                profile.kharma_points -= settings.KHARMA_POINTS_LOST['own_comment_disliked']
                profile.save()
                for vote in votes:
                    profile2 = vote.user.profile
                    if vote.vote == 1:
                        profile2.kharma_points-=settings.KHARMA_POINTS_LOST['voted_comment_unvalidated']
                    if vote.vote == 2:
                        profile2.kharma_points+=settings.KHARMA_POINTS_EARNED['voted_comment_validated']
                    profile2.save()