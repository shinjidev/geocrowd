from ordereddict import OrderedDict

from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.db.models.fields import BooleanField
from django.conf import settings
from django.core.urlresolvers import reverse

from places.models import Place
from comment import strings

class Comment(models.Model):

    description=models.TextField(
        verbose_name=strings.COMMENT_DESCRIPTION,
    )

    user=models.ForeignKey(User,
        verbose_name=strings.COMMENT_USER,
    )

    place=models.ForeignKey(Place,
        verbose_name=strings.COMMENT_PLACE,
    )

    likes = models.PositiveIntegerField(
        verbose_name=strings.COMMENT_LIKE,
        editable=False,
        default=0,
    )

    dislikes = models.PositiveIntegerField(
        verbose_name=strings.COMMENT_DISLIKE,
        editable=False,
        default=0,
    )

    date = models.DateTimeField(
    	verbose_name=strings.COMMENT_DATE,
		auto_now_add=True,
        editable=False,
    )

    votes_reached=BooleanField(
        verbose_name=strings.COMMENT_VOTES_REACHED,
        default=False,
    )

    dislikes_reached=BooleanField(
        verbose_name=strings.COMMENT_DISLIKES_REACHED,
        default=False,
    )

    visibility=BooleanField(
        verbose_name=strings.COMMENT_VISIBILITY,
        default=True,
    )

    class Meta:
        verbose_name = strings.COMMENT_VERBOSE_NAME
        verbose_name_plural = strings.COMMENT_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.description,)

    def get_likes_difference(self):
        return (self.likes - self.dislikes)

    def get_restful_url(self):
        return '%s%s' % (
            settings.API_V1_PREFIX.rstrip('/'),
            reverse('comments', kwargs={'comment_id':self.id})
        )

    def get_restful_link_metadata(self, rel='alternate'):
        metadata = OrderedDict()
        metadata['href'] = self.get_restful_url()
        metadata['rel'] = rel
        metadata['title'] = self.description
        metadata['type'] = 'application/json'
        return metadata

    @staticmethod
    def create_new_comment(
        description,
        user,
        place
        ):

        new_comment = Comment(
            description=description,
            user=user,
            place=place
        )
        new_comment.save()
        return new_comment

from common.datastructures.enumeration import Enumeration

class VoteComment(models.Model):
    user=models.ForeignKey(User,
        verbose_name=strings.COMMENT_USER,
    )

    comment=models.ForeignKey(Comment,
        verbose_name=strings.COMMENT_VERBOSE_NAME,
    )

    date = models.DateTimeField(
        verbose_name=strings.COMMENT_DATE,
        auto_now_add=True,
        editable=False,
    )

    VOTE_CHOICES = Enumeration([
        (1, 'like', strings.COMMENT_LIKE),
        (2, 'dislike', strings.COMMENT_DISLIKE),
    ])

    vote=models.PositiveIntegerField(
        verbose_name=strings.COMMENT_VOTE,
        choices=VOTE_CHOICES,
        default=1,
    )

    class Meta:
        verbose_name = strings.COMMENT_VOTE_VERBOSE_NAME
        verbose_name_plural = strings.COMMENT_VOTE_VERBOSE_NAME_PLURAL

    def __unicode__(self):
        return u'%s' % (self.VOTE_CHOICES.get_key(self.vote),)

    @staticmethod
    def create_new_vote(
        vote,
        user,
        comment
        ):

        vote_value=VoteComment.VOTE_CHOICES.get_value(vote)
        new_vote = VoteComment(
            vote=vote_value,
            user=user,
            comment=comment
        )
        new_vote.save()
        return new_vote