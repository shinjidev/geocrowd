from django.contrib import admin
from comment.models import Comment, VoteComment

class CommentAdmin(admin.ModelAdmin):
	list_display=(
		'__unicode__',
		'user',
		'place')
	readonly_fields=('likes','dislikes','date','votes_reached','dislikes_reached')

admin.site.register(Comment,CommentAdmin)

class VoteCommentAdmin(admin.ModelAdmin):
	list_display=(
		'__unicode__',
		'user',
		'comment')
	readonly_fields=('date',)

admin.site.register(VoteComment,VoteCommentAdmin)