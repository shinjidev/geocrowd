from django.conf.urls import patterns, include, url

from comment import resources

urlpatterns = patterns('',
    url(r'^add/$',
        resources.CommentController.as_view(),
        name='add'),
    url(r'^(?P<comment_id>[-\w]+)/like/$',
        resources.VoteCommentController.as_view(),
        name='like'),
    url(r'^$',
        resources.CommentCollection.as_view(),
        name='comments'),
	url(r'^(?P<comment_id>[-\w]+)/$',
        resources.CommentResource.as_view(),
        name='comments')
)