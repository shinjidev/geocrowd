from django.utils.translation import ugettext_lazy as _

COMMENT_MANDATORY_FIELDS = ['description','user','place']

VOTE_MANDATORY_FIELDS = ['like','user','comment']

COMMENT_DESCRIPTION = _(u'Description')
COMMENT_PLACE = _(u'Place')
COMMENT_USER = _(u'User')
COMMENT_LIKE = _(u'Likes')
COMMENT_DISLIKE = _(u'Dislikes')
COMMENT_VISIBILITY = _(u'Visibility')
COMMENT_DATE = _(u'Date')
COMMENT_VOTE = _(u'Vote')
COMMENT_VOTES_REACHED = _(u'Likes Reached')
COMMENT_DISLIKES_REACHED = _(u'Dislikes Reached')

COMMENT_VERBOSE_NAME = _(u'Comment')
COMMENT_VERBOSE_NAME_PLURAL = _(u'Comments')
COMMENT_VOTE_VERBOSE_NAME = _(u'Comment vote')
COMMENT_VOTE_VERBOSE_NAME_PLURAL = _(u'Comment votes')

NON_EXISTANT_COMMENT = _(u'Comment %s does not exist!')
COMMENT_EXISTS = _(u'You\'ve already made this comment for this place: %s!')
COMMENT_COLLECTION = _(u'A collection of all comments.')
INVOLVED_DONT_EXIST = _(u'The %s involved in the comment do(es)n\'t exist!')

NON_EXISTANT_USER = _(u'User %s does not exist!')